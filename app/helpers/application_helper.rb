module ApplicationHelper

    private 

    def attachment_recoilMath(start, col, attachment)
        recoil = 0
        attachment.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
           
            return recoil / attachment.length
        else 
            return 0
        end 
    end


    def attachment_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 

    def canon_net_recoil(weapon, attachment)
         valeu_net = weapon - attachment
         return valeu_net
    end


    def average_hip_fire(hipfires)
        area = 0       
        nb_of_spread = hipfires.count

        hipfires.each do |hip|
           area += hip.yaxis * hip.xaxis   
        end

       if nb_of_spread > 0
        moy = (area / nb_of_spread) / 1000
       end 
            
        return moy    
    end


    def net_hip_fire(hipfires, hip_recoil)
        area = 0       
        nb_of_spread = hipfires.count

        hipfires.each do |hip|
           area += hip.yaxis * hip.xaxis   
        end

       if nb_of_spread > 0
        moy = (area / nb_of_spread) / 1000
        net_value = (hip_recoil).abs - moy 
       end 
            
       return net_value
    end
end
