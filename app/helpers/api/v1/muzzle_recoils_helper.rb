module Api::V1::MuzzleRecoilsHelper

    private

    def muzzle_recoilMath(start, col)
        recoil = 0
        @muzzle_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @muzzle_recoils.length
        else 
            return 0
        end 
    end


    def muzzle_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 

end
