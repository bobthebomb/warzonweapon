module Api::V1::MunitionRecoilsHelper
    def munition_recoilMath(start, col)
        recoil = 0
        @munition_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @munition_recoils.length
        else 
            return 0
        end 
    end


    def munition_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 
end
