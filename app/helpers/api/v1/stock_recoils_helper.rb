module Api::V1::StockRecoilsHelper
    def stock_recoilMath(start, col)
        recoil = 0
        @stock_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @stock_recoils.length
        else 
            return 0
        end 
    end


    def stock_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 
end
