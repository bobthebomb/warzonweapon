module Api::V1::LaserRecoilsHelper

    def laser_recoilMath(start, col)
        recoil = 0
        @laser_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @laser_recoils.length
        else 
            return 0
        end 
    end


    def laser_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 
    
end
