module Api::V1::UnderbarrelRecoilsHelper
    def underbarrel_recoilMath(start, col)
        recoil = 0
        @underbarrel_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @underbarrel_recoils.length
        else 
            return 0
        end 
    end


    def underbarrel_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 
end
