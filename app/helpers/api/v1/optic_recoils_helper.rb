module Api::V1::OpticRecoilsHelper
    def optic_recoilMath(start, col)
        recoil = 0
        @optic_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @optic_recoils.length
        else 
            return 0
        end 
    end


    def optic_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 
end
