module Api::V1::RecoilsHelper

   
private

    def recoilMath(start, col)
        recoil = 0
        @recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @recoils.length
        else 
            return 0
        end 
    end


    def recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 

    

end
