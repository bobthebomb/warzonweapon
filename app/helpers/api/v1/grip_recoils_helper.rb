module Api::V1::GripRecoilsHelper
    def grip_recoilMath(start, col)
        recoil = 0
        @grip_recoils.each do |r|
           recoil_temp = r[col] - r[start]
           recoil += recoil_temp 
        end
        if recoil != 0
            return recoil / @grip_recoils.length
        else 
            return 0
        end 
    end


    def grip_recoilColumn(start, now)
        recoil = 0
        recoil = now - start

        return recoil
    end 
end
