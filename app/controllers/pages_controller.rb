class PagesController < ApplicationController
before_action :admincheck, only: [:admin, :user, :table]

    def home
    end

    def admin
        @admin = current_user
    end

    def reactor
    end

    def user
        @admin = current_user
        respond_to do |format|
            format.html
            format.json
        end
    end

    def table
        @weapons = Weapon.order(type_id: :asc)
    end



end
