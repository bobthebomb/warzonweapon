class BlogsController < ApplicationController
  before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
 

  POSTS_PER_PAGE = 12

  def index
    @page = params.fetch(:page, 0).to_i
    if @page < 0
      @page = 0
    end 
    @blogs = Blog.where(featured: 0).order(created_at: :desc).offset(@page * POSTS_PER_PAGE).limit(POSTS_PER_PAGE) 
    @bigfeature = Blog.where(featured: 1).last
    @smalltopfeature = Blog.where(featured: 2).last
    @smallbotfeature = Blog.where(featured: 3).last

  end
  
  def new
    @blog = Blog.new
  end

  def create
  @blog = Blog.new(blog_params)
    if @blog.save 
      redirect_to root_path
    else
      render "new", alert: @blog.errors.full_messages     
    end
  end

  def show
    @blog = Blog.friendly.find(params[:id])
  end

  def edit
    @blog = Blog.friendly.find(params[:id])
  end

  def update
    @blog = Blog.friendly.find(params[:id])
    if @blog.update(blog_params)
      redirect_to root_path
    else
      redirect_to edit_blog_path(@blog), alert: @blog.errors.full_messages     
    end
  end

  def destroy
    @blog = Blog.friendly.find(params[:id])
    @blog.destroy 
    redirect_to blogs_path
  end

 private 
 
 def blog_params
  params.require(:blog).permit(:title, :text_area_1, :illustration, :image_1, :featured, :description, :show_illustration, :body)
 end


 
end
