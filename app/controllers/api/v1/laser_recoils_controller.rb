class Api::V1::LaserRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @laser_recoil = @laser.laser_recoils.build
  end

  def create
    find_needed
    @laser_recoil = @laser.laser_recoils.build(recoil_params)
    if @laser_recoil.save 
      redirect_to api_v1_weapon_laser_laser_recoils_path(@weapon, @laser)
    else
      render "new"
    end
  end

  def index
    find_needed
    @laser_recoils = @laser.laser_recoils
  end

  def edit
    find_needed
    @laser_recoil = @laser.laser_recoils.find(params[:id])
  end

  def update
    find_needed
    @laser_recoil = @laser.laser_recoils.find(params[:id])
    if @laser_recoil.update(recoil_params)
      redirect_to api_v1_weapon_laser_laser_recoils_path(@weapon, @laser)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @laser_recoil = @laser.laser_recoils.find(params[:id])
    @laser_recoil.destroy
    redirect_to api_v1_weapon_laser_laser_recoils_path(@weapon, @laser)
  end

  private

  def recoil_params
    params.require(:laser_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @laser = Laser.find(params[:laser_id])
  end

  

end
