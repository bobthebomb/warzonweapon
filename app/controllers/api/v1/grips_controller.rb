class Api::V1::GripsController < ApplicationController



    
    before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
    before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}


    def index
    find_weapon
    @grips = @weapon.grips.all
    respond_to do |format|
        format.html
        format.json
    end
    end

    def new
    find_weapon
    @grip = @weapon.grips.build
    end

    def show
    find_weapon
    @grip = Grip.find(params[:id])
    respond_to do |format|
        format.html
        format.json
    end
    end

    def edit
        find_weapon
        @grip = Grip.find(params[:id])

        @hip_fires = @grip.hip_fires.all
        @hip_fire = @grip.hip_fires.new
    end

    def update
        find_weapon
        @grip = @weapon.grips.find(params[:id])
        if @grip.update(grip_params) 
            redirect_to api_v1_weapon_grips_path(@weapon)
        else
            render new
        end
    
    end


    def create
    find_weapon
    @grip = @weapon.grips.build(grip_params)
        if @grip.save 
            redirect_to api_v1_weapon_grips_path(@weapon)
        else
            render new
        end
    end
    
    private

    def find_weapon
        @weapon = Weapon.find(params[:weapon_id])
    end

    def grip_params
         params.require(:grip).permit(:name, :weapon_id, :rpm, :reload_time, :ads_time, :sprint_to_fire,
            :movement, :ads_movement, :hip_fire_cone, :bullet_velocity, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
         :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, 
         :head_dmg_range_1, :chest_dmg_range_1, :stomach_dmg_range_1, :limbs_dmg_range_1,
         :head_dmg_range_2, :chest_dmg_range_2, :stomach_dmg_range_2, :limbs_dmg_range_2,
         :head_dmg_range_3, :chest_dmg_range_3, :stomach_dmg_range_3, :limbs_dmg_range_3,
         :head_dmg_range_4, :chest_dmg_range_4, :stomach_dmg_range_4, :limbs_dmg_range_4,
        :spread_image, :image, :range_1, :range_2, :range_3, :range_4,  :range_3_used, :range_4_used, :game, :note  )
    end




end
