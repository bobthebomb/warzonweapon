class Api::V1::Canons::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Canon.find(params[:canon_id])
    end

end
