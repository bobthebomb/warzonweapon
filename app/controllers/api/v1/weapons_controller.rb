class Api::V1::WeaponsController < ApplicationController
    before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
    before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}

    def index
       find_type
        @weapons = Weapon.where(:type_id => @type.id)
        respond_to do |format|
            format.html
            format.json
        end
    end


    def show
        @weapon = Weapon.find(params[:id])
        respond_to do |format|
            format.html
            format.json
        end
       
    end

    def new  
        find_type
        @weapon = @type.weapons.new
    end

    def create
        find_type_in_create
        @weapon = @type.weapons.new(weapon_params)
        if @weapon.save
            redirect_to action: "index", id: @type.id
        else
            render "new"
        end
    
    end

    def edit
        @weapon = Weapon.find(params[:id])

        @hip_fires = @weapon.hip_fires.all
        @hip_fire = @weapon.hip_fires.new
    end

    def update
        @weapon = Weapon.find(params[:id])
        @type = @weapon.type
        if @weapon.update(weapon_params) 
            redirect_to action: "index", id: @type.id
        else
            render "edit"
        end
        
    end

    def destroy
        @type = Type.find(params[:type_id])
        @weapon = Weapon.find(params[:id])
        @weapon.destroy
        redirect_to action: "index", id: @type.id
    end
    

private

    def find_type
      @type = Type.find(params[:id])
    end

    def find_type_in_create
        @type = Type.find(params[:type_id])
      end

    def weapon_params
        params.require(:weapon).permit(:name, :type_id, :rpm, :magazine, :reload_time, :ads_time, :sprint_to_fire,
        :movement, :ads_movement, :hip_fire_cone, :bullet_velocity, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
     :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, 
     :head_dmg_range_1, :chest_dmg_range_1, :stomach_dmg_range_1, :limbs_dmg_range_1,
     :head_dmg_range_2, :chest_dmg_range_2, :stomach_dmg_range_2, :limbs_dmg_range_2,
     :head_dmg_range_3, :chest_dmg_range_3, :stomach_dmg_range_3, :limbs_dmg_range_3,
     :head_dmg_range_4, :chest_dmg_range_4, :stomach_dmg_range_4, :limbs_dmg_range_4,
    :spread_image, :image, :range_1, :range_2, :range_3, :range_4,  :range_3_used, :range_4_used, :game, :note, :content, :burst )
    end

end
