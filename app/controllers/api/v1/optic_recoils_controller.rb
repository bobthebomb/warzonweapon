class Api::V1::OpticRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @optic_recoil = @optic.optic_recoils.build
  end

  def create
    find_needed
    @optic_recoil = @optic.optic_recoils.build(recoil_params)
    if @optic_recoil.save 
      redirect_to api_v1_weapon_optic_optic_recoils_path(@weapon, @optic)
    else
      render "new"
    end
  end

  def index
    find_needed
    @optic_recoils = @optic.optic_recoils
  end

  def edit
    find_needed
    @optic_recoil = @optic.optic_recoils.find(params[:id])
  end

  def update
    find_needed
    @optic_recoil = @optic.optic_recoils.find(params[:id])
    if @optic_recoil.update(recoil_params)
      redirect_to api_v1_weapon_optic_optic_recoils_path(@weapon, @optic)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @optic_recoil = @optic.optic_recoils.find(params[:id])
    @optic_recoil.destroy
    redirect_to api_v1_weapon_optic_optic_recoils_path(@weapon, @optic)
  end

  private

  def recoil_params
    params.require(:optic_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @optic = Optic.find(params[:optic_id])
  end

  
end
