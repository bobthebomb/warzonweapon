class Api::V1::MunitionRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @munition_recoil = @munition.munition_recoils.build
  end

  def create
    find_needed
    @munition_recoil = @munition.munition_recoils.build(recoil_params)
    if @munition_recoil.save 
      redirect_to api_v1_weapon_munition_munition_recoils_path(@weapon, @munition)
    else
      render "new"
    end
  end

  def index
    find_needed
    @munition_recoils = @munition.munition_recoils
  end

  def edit
    find_needed
    @munition_recoil = @munition.munition_recoils.find(params[:id])
  end

  def update
    find_needed
    @munition_recoil = @munition.munition_recoils.find(params[:id])
    if @munition_recoil.update(recoil_params)
      redirect_to api_v1_weapon_munition_munition_recoils_path(@weapon, @munition)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @munition_recoil = @munition.munition_recoils.find(params[:id])
    @munition_recoil.destroy
    redirect_to api_v1_weapon_munition_munition_recoils_path(@weapon, @munition)
  end

  private

  def recoil_params
    params.require(:munition_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @munition = Munition.find(params[:munition_id])
  end

  
end
