class Api::V1::Munitions::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Munition.find(params[:munition_id])
    end

end
