class Api::V1::MunitionsController < ApplicationController

    before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
    before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}


    def index
    find_weapon
    @munitions = @weapon.munitions.all
    respond_to do |format|
        format.html
        format.json
    end
    end

    def new
    find_weapon
    @munition = @weapon.munitions.build
    end

    def show
    find_weapon
    @munition = Munition.find(params[:id])
    respond_to do |format|
        format.html
        format.json
    end
    end

    def edit
        find_weapon
        @munition = Munition.find(params[:id])

        @hip_fires = @munition.hip_fires.all
        @hip_fire = @munition.hip_fires.new
    end

    def update
        find_weapon
        @munition = @weapon.munitions.find(params[:id])
        if @munition.update(munition_params) 
            redirect_to api_v1_weapon_munitions_path(@weapon)
        else
            render new
        end
    
    end


    def create
    find_weapon
    @munition = @weapon.munitions.build(munition_params)
        if @munition.save 
            redirect_to api_v1_weapon_munitions_path(@weapon)
        else
            render new
        end
    end
    
    private

    def find_weapon
        @weapon = Weapon.find(params[:weapon_id])
    end

    def munition_params
         params.require(:munition).permit(:name, :weapon_id, :rpm, :reload_time, :ads_time, :sprint_to_fire,
            :movement, :ads_movement, :hip_fire_cone, :bullet_velocity, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
         :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, 
         :head_dmg_range_1, :chest_dmg_range_1, :stomach_dmg_range_1, :limbs_dmg_range_1,
         :head_dmg_range_2, :chest_dmg_range_2, :stomach_dmg_range_2, :limbs_dmg_range_2,
         :head_dmg_range_3, :chest_dmg_range_3, :stomach_dmg_range_3, :limbs_dmg_range_3,
         :head_dmg_range_4, :chest_dmg_range_4, :stomach_dmg_range_4, :limbs_dmg_range_4,
        :spread_image, :image, :range_1, :range_2, :range_3, :range_4,  :range_3_used, :range_4_used, :game, :note, :magazine  )
    end

end
