class Api::V1::Optics::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Optic.find(params[:optic_id])
    end

end
