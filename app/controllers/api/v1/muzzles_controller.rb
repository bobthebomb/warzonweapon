class Api::V1::MuzzlesController < ApplicationController

    
        before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
        before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}
    
    
        def index
        find_weapon
        @muzzles = @weapon.muzzles.all
        respond_to do |format|
            format.html
            format.json
        end
        end
    
        def new
        find_weapon
        @muzzle = @weapon.muzzles.build
        end
    
        def show
        find_weapon
        @muzzle = Muzzle.find(params[:id])
        respond_to do |format|
            format.html
            format.json
        end
        end
    
        def edit
            find_weapon
            @muzzle = Muzzle.find(params[:id])

            @hip_fires = @muzzle.hip_fires.all
            @hip_fire = @muzzle.hip_fires.new
        end
    
        def update
            find_weapon
            @muzzle = @weapon.muzzles.find(params[:id])
            if @muzzle.update(muzzle_params) 
                redirect_to api_v1_weapon_muzzles_path(@weapon)
            else
                render new
            end
        
        end
    
    
        def create
        find_weapon
        @muzzle = @weapon.muzzles.build(muzzle_params)
            if @muzzle.save 
                redirect_to api_v1_weapon_muzzles_path(@weapon)
            else
                render new
            end
        end
        
        private
    
        def find_weapon
            @weapon = Weapon.find(params[:weapon_id])
        end
    
        def muzzle_params
             params.require(:muzzle).permit(:name, :weapon_id, :rpm, :reload_time, :ads_time, :sprint_to_fire,
                :movement, :ads_movement, :hip_fire_cone, :bullet_velocity, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
             :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, 
             :head_dmg_range_1, :chest_dmg_range_1, :stomach_dmg_range_1, :limbs_dmg_range_1,
             :head_dmg_range_2, :chest_dmg_range_2, :stomach_dmg_range_2, :limbs_dmg_range_2,
             :head_dmg_range_3, :chest_dmg_range_3, :stomach_dmg_range_3, :limbs_dmg_range_3,
             :head_dmg_range_4, :chest_dmg_range_4, :stomach_dmg_range_4, :limbs_dmg_range_4,
            :spread_image, :image, :range_1, :range_2, :range_3, :range_4,  :range_3_used, :range_4_used, :game, :note  )
        end
    
    
    

    

end
