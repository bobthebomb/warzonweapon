class Api::V1::Weapons::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Weapon.find(params[:weapon_id])
    end

end
