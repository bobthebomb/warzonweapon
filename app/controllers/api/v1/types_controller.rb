class Api::V1::TypesController < ApplicationController
  before_action :admincheck, only: [:new, :create, :show, :edit, :update, :destroy]
  before_action :admincheck, only: [:index], unless:  -> {request.format.json?}

  def index
    @types = Type.all.order(id: :asc)
    respond_to do |format|
      format.html
      format.json

    end
  end
  
  def show
    @type = Type.find(params[:id])
    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    @type = Type.new
  end

  def create
    @type = Type.new(type_params)
      if @type.save 
        redirect_to api_v1_types_path
      else
        render new
      end
  end

  def edit 
    @type = Type.find(params[:id])
  end


  def update 
    @type = Type.find(params[:id])
    if @type.update(type_params) 
      redirect_to api_v1_types_path
    else
      render edit
    end
  end

  def destroy
    @type = Type.find(params[:id])
    @type.destroy
    redirect_to api_v1_types_path
  end

private

  def type_params
    params.require(:type).permit(:name, :game, :description)
  end

end
