class Api::V1::Grips::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Grip.find(params[:grip_id])
    end

end
