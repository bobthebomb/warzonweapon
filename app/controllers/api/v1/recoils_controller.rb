class Api::V1::RecoilsController < ApplicationController
  before_action :admincheck

  def index
    find_weapon
    @recoils = @weapon.recoils.all
  end

  def new
  find_weapon
   @recoil = @weapon.recoils.build
  end

  def create
    find_weapon
    @recoil = @weapon.recoils.build(recoil_params)
    if @recoil.save 
      redirect_to api_v1_weapon_recoils_path(@weapon)
    else
      render new
    end
  end

  def edit
    find_weapon
    @recoil = @weapon.recoils.find(params[:id])
  end

  def update
    find_weapon
    @recoil = @weapon.recoils.find(params[:id])
    if @recoil.update(recoil_params) 
      redirect_to api_v1_weapon_recoils_path(@weapon)
    else 
      render edit
    end
  end

  def destroy
    find_weapon
    @recoil = @weapon.recoils.find(params[:id])
    @recoil.destroy
    redirect_to api_v1_weapon_recoils_path(@weapon)
  end


  private

  def recoil_params
    params.require(:recoil).permit(:name, :hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_weapon
    @weapon = Weapon.find(params[:weapon_id])
end

end
