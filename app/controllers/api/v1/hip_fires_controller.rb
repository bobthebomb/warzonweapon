class Api::V1::HipFiresController < ApplicationController

before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}


def create
    @hip_fire = @attachement.hip_fires.new(hip_fires_params)
    if @hip_fire.save
    redirect_to api_v1_weapon_path(@weapon)
    else 
    redirect_to api_v1_weapon_path(@weapon)
    end
end

def destroy
    @hip_fire = @attachement.hip_fires.find(params[:id])
    if @hip_fire.destroy
        flash.alert  = "Recoil Deleted"
    end 
end


private 

    def hip_fires_params
        params.require(:hip_fire).permit(:xaxis, :yaxis )
    end

end