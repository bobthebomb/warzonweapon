class Api::V1::StockRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @stock_recoil = @stock.stock_recoils.build
  end

  def create
    find_needed
    @stock_recoil = @stock.stock_recoils.build(recoil_params)
    if @stock_recoil.save 
      redirect_to api_v1_weapon_stock_stock_recoils_path(@weapon, @stock)
    else
      render "new"
    end
  end

  def index
    find_needed
    @stock_recoils = @stock.stock_recoils
  end

  def edit
    find_needed
    @stock_recoil = @stock.stock_recoils.find(params[:id])
  end

  def update
    find_needed
    @stock_recoil = @stock.stock_recoils.find(params[:id])
    if @stock_recoil.update(recoil_params)
      redirect_to api_v1_weapon_stock_stock_recoils_path(@weapon, @stock)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @stock_recoil = @stock.stock_recoils.find(params[:id])
    @stock_recoil.destroy
    redirect_to api_v1_weapon_stock_stock_recoils_path(@weapon, @stock)
  end

  private

  def recoil_params
    params.require(:stock_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @stock = Stock.find(params[:stock_id])
  end
end
