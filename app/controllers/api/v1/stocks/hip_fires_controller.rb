class Api::V1::Stocks::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Stock.find(params[:stock_id])
    end

end
