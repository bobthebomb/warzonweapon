class Api::V1::GripRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @grip_recoil = @grip.grip_recoils.build
  end

  def create
    find_needed
    @grip_recoil = @grip.grip_recoils.build(recoil_params)
    if @grip_recoil.save 
      redirect_to api_v1_weapon_grip_grip_recoils_path(@weapon, @grip)
    else
      render "new"
    end
  end

  def index
    find_needed
    @grip_recoils = @grip.grip_recoils
  end

  def edit
    find_needed
    @grip_recoil = @grip.grip_recoils.find(params[:id])
  end

  def update
    find_needed
    @grip_recoil = @grip.grip_recoils.find(params[:id])
    if @grip_recoil.update(recoil_params)
      redirect_to api_v1_weapon_grip_grip_recoils_path(@weapon, @grip)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @grip_recoil = @grip.grip_recoils.find(params[:id])
    @grip_recoil.destroy
    redirect_to api_v1_weapon_grip_grip_recoils_path(@weapon, @grip)
  end

  private

  def recoil_params
    params.require(:grip_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @grip = Grip.find(params[:grip_id])
  end

  
end
