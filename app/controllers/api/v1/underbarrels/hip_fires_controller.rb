class Api::V1::Underbarrels::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Underbarrel.find(params[:underbarrel_id])
    end

end
