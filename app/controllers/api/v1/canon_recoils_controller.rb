class Api::V1::CanonRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @canon_recoil = @canon.canon_recoils.build
  end

  def create
    find_needed
    @canon_recoil = @canon.canon_recoils.build(recoil_params)
    if @canon_recoil.save 
      redirect_to api_v1_weapon_canon_canon_recoils_path(@weapon, @canon)
    else
      render "new"
    end
  end

  def index
    find_needed
    @canon_recoils = @canon.canon_recoils
  end

  def edit
    find_needed
    @canon_recoil = @canon.canon_recoils.find(params[:id])
  end

  def update
    find_needed
    @canon_recoil = @canon.canon_recoils.find(params[:id])
    if @canon_recoil.update(recoil_params)
      redirect_to api_v1_weapon_canon_canon_recoils_path(@weapon, @canon)
    else
      render "edit"
    end
  end

  def destroy
    find_needed
    @canon_recoil = @canon.canon_recoils.find(params[:id])
    @canon_recoil.destroy
    redirect_to api_v1_weapon_canon_canon_recoils_path(@weapon, @canon)
  end

  private

  def recoil_params
    params.require(:canon_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @canon = Canon.find(params[:canon_id])
  end

  


end
