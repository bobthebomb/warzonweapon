class Api::V1::UnderbarrelRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @underbarrel_recoil = @underbarrel.underbarrel_recoils.build
  end

  def create
    find_needed
    @underbarrel_recoil = @underbarrel.underbarrel_recoils.build(recoil_params)
    if @underbarrel_recoil.save 
      redirect_to api_v1_weapon_underbarrel_underbarrel_recoils_path(@weapon, @underbarrel)
    else
      render "new"
    end
  end

  def index
    find_needed
    @underbarrel_recoils = @underbarrel.underbarrel_recoils
  end

  def edit
    find_needed
    @underbarrel_recoil = @underbarrel.underbarrel_recoils.find(params[:id])
  end

  def update
    find_needed
    @underbarrel_recoil = @underbarrel.underbarrel_recoils.find(params[:id])
    if @underbarrel_recoil.update(recoil_params)
      redirect_to api_v1_weapon_underbarrel_underbarrel_recoils_path(@weapon, @underbarrel)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @underbarrel_recoil = @underbarrel.underbarrel_recoils.find(params[:id])
    @underbarrel_recoil.destroy
    redirect_to api_v1_weapon_underbarrel_underbarrel_recoils_path(@weapon, @underbarrel)
  end

  private

  def recoil_params
    params.require(:underbarrel_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @underbarrel = Underbarrel.find(params[:underbarrel_id])
  end

  
end
