class Api::V1::Muzzles::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Muzzle.find(params[:muzzle_id])
    end

end
