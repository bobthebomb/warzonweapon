class Api::V1::SetupsController < ApplicationController
     before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
     before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}

     protect_from_forgery with: :null_session,
    only: Proc.new { |c| c.request.format.json? }



    def index
    @setups = Setup.all
    end

    def show 
        @setup = Setup.find(params[:id])
    end

    def new 
        @setup = Setup.new
    end

    def create 
        @setup = Setup.new(setup_params)
        if @setup.save
            redirect_to api_v1_setups_path
        else
            render "new"
        end
    end

    def edit
        @setup = Setup.find(params[:id])
    end

    def update
        @setup = Setup.find(params[:id])
        if @setup.update(setup_params)
            redirect_to api_v1_setups_path
        else
            render "edit"
        end
    end

    def destroy
        @setup = Setup.find(params[:id])
        @setup.destroy
        redirect_to api_v1_setups_path
    end 


    private

    def setup_params
        params.require(:setup).permit(:name, :weapon_id, :rpm, :reload_time, :ads_time, :sprint_to_fire, :meta_id, :active, :burst,
            :movement, :ads_movement, :hip_fire_cone, :bullet_velocity, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
         :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, 
         :head_dmg_range_1, :chest_dmg_range_1, :stomach_dmg_range_1, :limbs_dmg_range_1,
         :head_dmg_range_2, :chest_dmg_range_2, :stomach_dmg_range_2, :limbs_dmg_range_2,
         :head_dmg_range_3, :chest_dmg_range_3, :stomach_dmg_range_3, :limbs_dmg_range_3,
         :head_dmg_range_4, :chest_dmg_range_4, :stomach_dmg_range_4, :limbs_dmg_range_4,
        :spread_image, :image, :range_1, :range_2, :range_3, :range_4,  :range_3_used, :range_4_used, :game, :note, :magazine, attachement_names: [] )

    end

end


