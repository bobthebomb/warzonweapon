class Api::V1::MuzzleRecoilsController < ApplicationController
  before_action :admincheck

  def new
    find_needed
    @muzzle_recoil = @muzzle.muzzle_recoils.build
  end

  def create
    find_needed
    @muzzle_recoil = @muzzle.muzzle_recoils.build(recoil_params)
    if @muzzle_recoil.save 
      redirect_to api_v1_weapon_muzzle_muzzle_recoils_path(@weapon, @muzzle)
    else
      render "new"
    end
  end

  def index
    find_needed
    @muzzle_recoils = @muzzle.muzzle_recoils
  end

  def edit
    find_needed
    @muzzle_recoil = @muzzle.muzzle_recoils.find(params[:id])
  end

  def update
    find_needed
    @muzzle_recoil = @muzzle.muzzle_recoils.find(params[:id])
    if @muzzle_recoil.update(recoil_params)
      redirect_to api_v1_weapon_muzzle_muzzle_recoils_path(@weapon, @muzzle)
    else
      render "new"
    end
  end

  def destroy
    find_needed
    @muzzle_recoil = @muzzle.muzzle_recoils.find(params[:id])
    @muzzle_recoil.destroy
    redirect_to api_v1_weapon_muzzle_muzzle_recoils_path(@weapon, @muzzle)
  end

  private

  def recoil_params
    params.require(:muzzle_recoil).permit(:hip_fire_cone, :bounce, :vertical_recoil, :horizontal_recoil, :vertical_recoil_first_10,
      :horizontal_recoil_first_10, :vertical_recoil_first_20, :horizontal_recoil_first_20, :vertical_recoil_first_0, :horizontal_recoil_first_0 )
  end

  def find_needed
  @weapon = Weapon.find(params[:weapon_id])
  @muzzle = Muzzle.find(params[:muzzle_id])
  end

  
end
