class Api::V1::PerksController < ApplicationController


    
    before_action :admincheck, only: [:new, :create, :edit, :update, :destroy]
    before_action :admincheck, only: [:index, :show], unless:  -> {request.format.json?}


    def index
    find_weapon
    @perks = @weapon.perks.all
    respond_to do |format|
        format.html
        format.json
    end
    end

    def new
    find_weapon
    @perk = @weapon.perks.build
    end

    def show
    find_weapon
    @perk = Perk.find(params[:id])
    respond_to do |format|
        format.html
        format.json
    end
    end

    def edit
        find_weapon
        @perk = Perk.find(params[:id])
    end

    def update
        find_weapon
        @perk = @weapon.perks.find(params[:id])
        if @perk.update(perk_params) 
            redirect_to api_v1_weapon_perks_path(@weapon)
        else
            render new
        end
    
    end


    def create
    find_weapon
    @perk = @weapon.perks.build(perk_params)
        if @perk.save 
            redirect_to api_v1_weapon_perks_path(@weapon)
        else
            render new
        end
    end
    
    private

    def find_weapon
        @weapon = Weapon.find(params[:weapon_id])
    end

    def perk_params
         params.require(:perk).permit(:name, :effect)
    end






end
