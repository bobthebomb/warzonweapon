class Api::V1::Lasers::HipFiresController < Api::V1::HipFiresController
    before_action :set_attachement

    private

    def set_attachement
        @attachement = Laser.find(params[:laser_id])
    end

end
