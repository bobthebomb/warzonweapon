class ApplicationController < ActionController::Base




    private

    def admincheck
        if authenticate_user! && current_user.admin 
            return true
        else
            flash[:error] = "No admin "
            redirect_to root_path
        end
    end




end
