json.underbarrels @underbarrels do |underbarrel|

json.id underbarrel.id
json.name underbarrel.name
json.rpm underbarrel.rpm
json.reload_time underbarrel.reload_time
json.ads_time underbarrel.ads_time
json.sprint_to_fire underbarrel.sprint_to_fire
json.movement underbarrel.movement
json.ads_movement underbarrel.ads_movement
json.hip_fire_cone underbarrel.hip_fire_cone
json.bullet_velocity underbarrel.bullet_velocity
json.bounce underbarrel.bounce
json.vertical_recoil underbarrel.vertical_recoil
json.horizontal_recoil underbarrel.horizontal_recoil
json.vertical_recoil_first_10 underbarrel.vertical_recoil_first_10
json.horizontal_recoil_first_10 underbarrel.horizontal_recoil_first_10
json.vertical_recoil_first_20 underbarrel.vertical_recoil_first_20
json.horizontal_recoil_first_20 underbarrel.horizontal_recoil_first_20
json.range_1 underbarrel.range_1
json.range_2 underbarrel.range_2
json.range_3 underbarrel.range_3
json.range_4 underbarrel.range_4
json.range_3_used underbarrel.range_3_used
json.range_4_used underbarrel.range_4_used
json.head_dmg_range_1 underbarrel.head_dmg_range_1
json.chest_dmg_range_1 underbarrel.chest_dmg_range_1
json.stomach_dmg_range_1 underbarrel.stomach_dmg_range_1
json.limbs_dmg_range_1 underbarrel.limbs_dmg_range_1
json.head_dmg_range_2 underbarrel.head_dmg_range_2
json.chest_dmg_range_2 underbarrel.chest_dmg_range_2
json.stomach_dmg_range_2 underbarrel.stomach_dmg_range_2
json.limbs_dmg_range_2 underbarrel.limbs_dmg_range_2
json.head_dmg_range_3 underbarrel.head_dmg_range_3
json.chest_dmg_range_3 underbarrel.chest_dmg_range_3
json.stomach_dmg_range_3 underbarrel.stomach_dmg_range_3
json.limbs_dmg_range_3 underbarrel.limbs_dmg_range_3
json.head_dmg_range_4 underbarrel.head_dmg_range_4
json.chest_dmg_range_4 underbarrel.chest_dmg_range_4
json.stomach_dmg_range_4 underbarrel.stomach_dmg_range_4
json.limbs_dmg_range_4 underbarrel.limbs_dmg_range_4
json.spread_image underbarrel.spread_image
json.game underbarrel.game
json.note underbarrel.note

end