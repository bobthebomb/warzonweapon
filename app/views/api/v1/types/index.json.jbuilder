    json.weapontype @types do |type|
        json.id type.id
        json.name type.name
        json.game type.game
        json.description type.description
    end