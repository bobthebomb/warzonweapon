json.stock  do 
    stock = @stock
    json.id stock.id
    json.name stock.name
    json.rpm stock.rpm
    json.reload_time stock.reload_time
    json.ads_time stock.ads_time
    json.sprint_to_fire stock.sprint_to_fire
    json.movement stock.movement
    json.ads_movement stock.ads_movement
    json.hip_fire_cone stock.hip_fire_cone
    json.bullet_velocity stock.bullet_velocity
    json.bounce stock.bounce
    json.vertical_recoil stock.vertical_recoil
    json.horizontal_recoil stock.horizontal_recoil
    json.vertical_recoil_first_10 stock.vertical_recoil_first_10
    json.horizontal_recoil_first_10 stock.horizontal_recoil_first_10
    json.vertical_recoil_first_20 stock.vertical_recoil_first_20
    json.horizontal_recoil_first_20 stock.horizontal_recoil_first_20
    json.range_1 stock.range_1
    json.range_2 stock.range_2
    json.range_3 stock.range_3
    json.range_4 stock.range_4
    json.range_3_used stock.range_3_used
    json.range_4_used stock.range_4_used
    json.head_dmg_range_1 stock.head_dmg_range_1
    json.chest_dmg_range_1 stock.chest_dmg_range_1
    json.stomach_dmg_range_1 stock.stomach_dmg_range_1
    json.limbs_dmg_range_1 stock.limbs_dmg_range_1
    json.head_dmg_range_2 stock.head_dmg_range_2
    json.chest_dmg_range_2 stock.chest_dmg_range_2
    json.stomach_dmg_range_2 stock.stomach_dmg_range_2
    json.limbs_dmg_range_2 stock.limbs_dmg_range_2
    json.head_dmg_range_3 stock.head_dmg_range_3
    json.chest_dmg_range_3 stock.chest_dmg_range_3
    json.stomach_dmg_range_3 stock.stomach_dmg_range_3
    json.limbs_dmg_range_3 stock.limbs_dmg_range_3
    json.head_dmg_range_4 stock.head_dmg_range_4
    json.chest_dmg_range_4 stock.chest_dmg_range_4
    json.stomach_dmg_range_4 stock.stomach_dmg_range_4
    json.limbs_dmg_range_4 stock.limbs_dmg_range_4
    json.spread_image stock.spread_image
    json.game stock.game
    json.note stock.note
end
    