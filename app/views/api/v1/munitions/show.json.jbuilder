json.munition  do 
    munition = @munition
    json.id munition.id
    json.name munition.name
    json.rpm munition.rpm
    json.reload_time munition.reload_time
    json.ads_time munition.ads_time
    json.sprint_to_fire munition.sprint_to_fire
    json.movement munition.movement
    json.ads_movement munition.ads_movement
    json.hip_fire_cone munition.hip_fire_cone
    json.bullet_velocity munition.bullet_velocity
    json.bounce munition.bounce
    json.vertical_recoil munition.vertical_recoil
    json.horizontal_recoil munition.horizontal_recoil
    json.vertical_recoil_first_10 munition.vertical_recoil_first_10
    json.horizontal_recoil_first_10 munition.horizontal_recoil_first_10
    json.vertical_recoil_first_20 munition.vertical_recoil_first_20
    json.horizontal_recoil_first_20 munition.horizontal_recoil_first_20
    json.range_1 munition.range_1
    json.range_2 munition.range_2
    json.range_3 munition.range_3
    json.range_4 munition.range_4
    json.range_3_used munition.range_3_used
    json.range_4_used munition.range_4_used
    json.head_dmg_range_1 munition.head_dmg_range_1
    json.chest_dmg_range_1 munition.chest_dmg_range_1
    json.stomach_dmg_range_1 munition.stomach_dmg_range_1
    json.limbs_dmg_range_1 munition.limbs_dmg_range_1
    json.head_dmg_range_2 munition.head_dmg_range_2
    json.chest_dmg_range_2 munition.chest_dmg_range_2
    json.stomach_dmg_range_2 munition.stomach_dmg_range_2
    json.limbs_dmg_range_2 munition.limbs_dmg_range_2
    json.head_dmg_range_3 munition.head_dmg_range_3
    json.chest_dmg_range_3 munition.chest_dmg_range_3
    json.stomach_dmg_range_3 munition.stomach_dmg_range_3
    json.limbs_dmg_range_3 munition.limbs_dmg_range_3
    json.head_dmg_range_4 munition.head_dmg_range_4
    json.chest_dmg_range_4 munition.chest_dmg_range_4
    json.stomach_dmg_range_4 munition.stomach_dmg_range_4
    json.limbs_dmg_range_4 munition.limbs_dmg_range_4
    json.spread_image munition.spread_image
    json.game munition.game
    json.note munition.note
    json.magazine munition.magazine

end
    