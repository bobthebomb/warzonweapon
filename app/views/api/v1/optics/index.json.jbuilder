json.optics @optics do |optic|

json.id optic.id
json.name optic.name
json.rpm optic.rpm
json.reload_time optic.reload_time
json.ads_time optic.ads_time
json.sprint_to_fire optic.sprint_to_fire
json.movement optic.movement
json.ads_movement optic.ads_movement
json.hip_fire_cone optic.hip_fire_cone
json.bullet_velocity optic.bullet_velocity
json.bounce optic.bounce
json.vertical_recoil optic.vertical_recoil
json.horizontal_recoil optic.horizontal_recoil
json.vertical_recoil_first_10 optic.vertical_recoil_first_10
json.horizontal_recoil_first_10 optic.horizontal_recoil_first_10
json.vertical_recoil_first_20 optic.vertical_recoil_first_20
json.horizontal_recoil_first_20 optic.horizontal_recoil_first_20
json.range_1 optic.range_1
json.range_2 optic.range_2
json.range_3 optic.range_3
json.range_4 optic.range_4
json.range_3_used optic.range_3_used
json.range_4_used optic.range_4_used
json.head_dmg_range_1 optic.head_dmg_range_1
json.chest_dmg_range_1 optic.chest_dmg_range_1
json.stomach_dmg_range_1 optic.stomach_dmg_range_1
json.limbs_dmg_range_1 optic.limbs_dmg_range_1
json.head_dmg_range_2 optic.head_dmg_range_2
json.chest_dmg_range_2 optic.chest_dmg_range_2
json.stomach_dmg_range_2 optic.stomach_dmg_range_2
json.limbs_dmg_range_2 optic.limbs_dmg_range_2
json.head_dmg_range_3 optic.head_dmg_range_3
json.chest_dmg_range_3 optic.chest_dmg_range_3
json.stomach_dmg_range_3 optic.stomach_dmg_range_3
json.limbs_dmg_range_3 optic.limbs_dmg_range_3
json.head_dmg_range_4 optic.head_dmg_range_4
json.chest_dmg_range_4 optic.chest_dmg_range_4
json.stomach_dmg_range_4 optic.stomach_dmg_range_4
json.limbs_dmg_range_4 optic.limbs_dmg_range_4
json.spread_image optic.spread_image
json.game optic.game
json.note optic.note

end