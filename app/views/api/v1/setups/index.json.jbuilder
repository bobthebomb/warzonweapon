json.setups @setups do |setups|

if setups.active  

json.id setups.id
json.name setups.name
json.rpm setups.rpm
json.burst setups.burst
json.reload_time setups.reload_time
json.ads_time setups.ads_time
json.sprint_to_fire setups.sprint_to_fire
json.movement setups.movement
json.ads_movement setups.ads_movement
json.hip_fire_cone setups.hip_fire_cone
json.bullet_velocity setups.bullet_velocity
json.bounce setups.bounce
json.vertical_recoil setups.vertical_recoil
json.horizontal_recoil setups.horizontal_recoil
json.vertical_recoil_first_10 setups.vertical_recoil_first_10
json.horizontal_recoil_first_10 setups.horizontal_recoil_first_10
json.vertical_recoil_first_20 setups.vertical_recoil_first_20
json.horizontal_recoil_first_20 setups.horizontal_recoil_first_20
json.range_1 setups.range_1
json.range_2 setups.range_2
json.range_3 setups.range_3
json.range_4 setups.range_4
json.range_3_used setups.range_3_used
json.range_4_used setups.range_4_used
json.head_dmg_range_1 setups.head_dmg_range_1
json.chest_dmg_range_1 setups.chest_dmg_range_1
json.stomach_dmg_range_1 setups.stomach_dmg_range_1
json.limbs_dmg_range_1 setups.limbs_dmg_range_1
json.head_dmg_range_2 setups.head_dmg_range_2
json.chest_dmg_range_2 setups.chest_dmg_range_2
json.stomach_dmg_range_2 setups.stomach_dmg_range_2
json.limbs_dmg_range_2 setups.limbs_dmg_range_2
json.head_dmg_range_3 setups.head_dmg_range_3
json.chest_dmg_range_3 setups.chest_dmg_range_3
json.stomach_dmg_range_3 setups.stomach_dmg_range_3
json.limbs_dmg_range_3 setups.limbs_dmg_range_3
json.head_dmg_range_4 setups.head_dmg_range_4
json.chest_dmg_range_4 setups.chest_dmg_range_4
json.stomach_dmg_range_4 setups.stomach_dmg_range_4
json.limbs_dmg_range_4 setups.limbs_dmg_range_4
json.spread_image setups.spread_image
json.game setups.game
json.note setups.note
json.magazine setups.magazine
json.meta_id setups.meta_id
json.active setups.active
json.attachement_names setups.attachement_names
    end
end 