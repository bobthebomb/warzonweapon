json.canons @canons do |canon|

json.id canon.id
json.name canon.name
json.rpm canon.rpm
json.reload_time canon.reload_time
json.ads_time canon.ads_time
json.sprint_to_fire canon.sprint_to_fire
json.movement canon.movement
json.ads_movement canon.ads_movement
json.hip_fire_cone canon.hip_fire_cone
json.bullet_velocity canon.bullet_velocity
json.bounce canon.bounce
json.vertical_recoil canon.vertical_recoil
json.horizontal_recoil canon.horizontal_recoil
json.vertical_recoil_first_10 canon.vertical_recoil_first_10
json.horizontal_recoil_first_10 canon.horizontal_recoil_first_10
json.vertical_recoil_first_20 canon.vertical_recoil_first_20
json.horizontal_recoil_first_20 canon.horizontal_recoil_first_20
json.range_1 canon.range_1
json.range_2 canon.range_2
json.range_3 canon.range_3
json.range_4 canon.range_4
json.range_3_used canon.range_3_used
json.range_4_used canon.range_4_used
json.head_dmg_range_1 canon.head_dmg_range_1
json.chest_dmg_range_1 canon.chest_dmg_range_1
json.stomach_dmg_range_1 canon.stomach_dmg_range_1
json.limbs_dmg_range_1 canon.limbs_dmg_range_1
json.head_dmg_range_2 canon.head_dmg_range_2
json.chest_dmg_range_2 canon.chest_dmg_range_2
json.stomach_dmg_range_2 canon.stomach_dmg_range_2
json.limbs_dmg_range_2 canon.limbs_dmg_range_2
json.head_dmg_range_3 canon.head_dmg_range_3
json.chest_dmg_range_3 canon.chest_dmg_range_3
json.stomach_dmg_range_3 canon.stomach_dmg_range_3
json.limbs_dmg_range_3 canon.limbs_dmg_range_3
json.head_dmg_range_4 canon.head_dmg_range_4
json.chest_dmg_range_4 canon.chest_dmg_range_4
json.stomach_dmg_range_4 canon.stomach_dmg_range_4
json.limbs_dmg_range_4 canon.limbs_dmg_range_4
json.spread_image canon.spread_image
json.game canon.game
json.note canon.note

end