
json.weapon do 
 weapon = @weapon
json.id weapon.id
json.name weapon.name
json.rpm weapon.rpm
json.burst weapon.burst
json.magazine weapon.magazine
json.reload_time weapon.reload_time
json.ads_time weapon.ads_time
json.sprint_to_fire weapon.sprint_to_fire
json.movement weapon.movement
json.ads_movement weapon.ads_movement
json.hip_fire_cone weapon.hip_fire_cone
json.bullet_velocity weapon.bullet_velocity
json.bounce weapon.bounce
json.vertical_recoil weapon.vertical_recoil
json.horizontal_recoil weapon.horizontal_recoil
json.vertical_recoil_first_10 weapon.vertical_recoil_first_10
json.horizontal_recoil_first_10 weapon.horizontal_recoil_first_10
json.vertical_recoil_first_20 weapon.vertical_recoil_first_20
json.horizontal_recoil_first_20 weapon.horizontal_recoil_first_20
json.range_1 weapon.range_1
json.range_2 weapon.range_2
json.range_3 weapon.range_3
json.range_4 weapon.range_4
json.range_3_used weapon.range_3_used
json.range_4_used weapon.range_4_used
json.head_dmg_range_1 weapon.head_dmg_range_1
json.chest_dmg_range_1 weapon.chest_dmg_range_1
json.stomach_dmg_range_1 weapon.stomach_dmg_range_1
json.limbs_dmg_range_1 weapon.limbs_dmg_range_1
json.head_dmg_range_2 weapon.head_dmg_range_2
json.chest_dmg_range_2 weapon.chest_dmg_range_2
json.stomach_dmg_range_2 weapon.stomach_dmg_range_2
json.limbs_dmg_range_2 weapon.limbs_dmg_range_2
json.head_dmg_range_3 weapon.head_dmg_range_3
json.chest_dmg_range_3 weapon.chest_dmg_range_3
json.stomach_dmg_range_3 weapon.stomach_dmg_range_3
json.limbs_dmg_range_3 weapon.limbs_dmg_range_3
json.head_dmg_range_4 weapon.head_dmg_range_4
json.chest_dmg_range_4 weapon.chest_dmg_range_4
json.stomach_dmg_range_4 weapon.stomach_dmg_range_4
json.limbs_dmg_range_4 weapon.limbs_dmg_range_4
json.spread_image weapon.spread_image
json.game weapon.game
json.note weapon.note
json.content weapon.content

if weapon.image.attached?
json.image url_for(weapon.image.variant(resize_to_limit: [600, 600], quality: 50))
end
end


