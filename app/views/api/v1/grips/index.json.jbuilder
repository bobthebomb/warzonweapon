json.grips @grips do |grip|

json.id grip.id
json.name grip.name
json.rpm grip.rpm
json.reload_time grip.reload_time
json.ads_time grip.ads_time
json.sprint_to_fire grip.sprint_to_fire
json.movement grip.movement
json.ads_movement grip.ads_movement
json.hip_fire_cone grip.hip_fire_cone
json.bullet_velocity grip.bullet_velocity
json.bounce grip.bounce
json.vertical_recoil grip.vertical_recoil
json.horizontal_recoil grip.horizontal_recoil
json.vertical_recoil_first_10 grip.vertical_recoil_first_10
json.horizontal_recoil_first_10 grip.horizontal_recoil_first_10
json.vertical_recoil_first_20 grip.vertical_recoil_first_20
json.horizontal_recoil_first_20 grip.horizontal_recoil_first_20
json.range_1 grip.range_1
json.range_2 grip.range_2
json.range_3 grip.range_3
json.range_4 grip.range_4
json.range_3_used grip.range_3_used
json.range_4_used grip.range_4_used
json.head_dmg_range_1 grip.head_dmg_range_1
json.chest_dmg_range_1 grip.chest_dmg_range_1
json.stomach_dmg_range_1 grip.stomach_dmg_range_1
json.limbs_dmg_range_1 grip.limbs_dmg_range_1
json.head_dmg_range_2 grip.head_dmg_range_2
json.chest_dmg_range_2 grip.chest_dmg_range_2
json.stomach_dmg_range_2 grip.stomach_dmg_range_2
json.limbs_dmg_range_2 grip.limbs_dmg_range_2
json.head_dmg_range_3 grip.head_dmg_range_3
json.chest_dmg_range_3 grip.chest_dmg_range_3
json.stomach_dmg_range_3 grip.stomach_dmg_range_3
json.limbs_dmg_range_3 grip.limbs_dmg_range_3
json.head_dmg_range_4 grip.head_dmg_range_4
json.chest_dmg_range_4 grip.chest_dmg_range_4
json.stomach_dmg_range_4 grip.stomach_dmg_range_4
json.limbs_dmg_range_4 grip.limbs_dmg_range_4
json.spread_image grip.spread_image
json.game grip.game
json.note grip.note

end