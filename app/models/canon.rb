class Canon < ApplicationRecord
  belongs_to :weapon
  has_many :canon_recoils
  has_many :hip_fires, as: :hipfireable

end
