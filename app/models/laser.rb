class Laser < ApplicationRecord
  belongs_to :weapon
  has_many :laser_recoils
  has_many :hip_fires, as: :hipfireable

end
