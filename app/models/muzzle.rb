class Muzzle < ApplicationRecord
  belongs_to :weapon
  has_many :muzzle_recoils
  has_many :hip_fires, as: :hipfireable

end
