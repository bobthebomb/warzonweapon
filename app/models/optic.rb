class Optic < ApplicationRecord
  belongs_to :weapon
  has_many :optic_recoils
  has_many :hip_fires, as: :hipfireable

end
