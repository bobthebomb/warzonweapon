class Grip < ApplicationRecord
  belongs_to :weapon
  has_many :grip_recoils
  has_many :hip_fires, as: :hipfireable

end
