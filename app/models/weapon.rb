class Weapon < ApplicationRecord
  has_many :hip_fires, as: :hipfireable

  belongs_to :type
  has_many :canons, dependent: :destroy
  has_many :muzzles, dependent: :destroy
  has_many :lasers, dependent: :destroy
  has_many :optics, dependent: :destroy
  has_many :stocks, dependent: :destroy
  has_many :underbarrels, dependent: :destroy
  has_many :munitions, dependent: :destroy
  has_many :grips, dependent: :destroy
  has_many :perks, dependent: :destroy
  has_many :recoils, dependent: :destroy

  validates :type_id, presence: true
  validates :name, presence: true
  validates :ads_time, numericality: {less_than_or_equal_to: 0}
  validates :hip_fire_cone, numericality: {less_than_or_equal_to: 0}
  validates :sprint_to_fire, numericality: {less_than_or_equal_to: 0}
  validates :bounce, numericality: {less_than_or_equal_to: 0}
  validates :vertical_recoil, numericality: {less_than_or_equal_to: 0}
  validates :horizontal_recoil, numericality: {less_than_or_equal_to: 0}
  validates :vertical_recoil_first_10, numericality: {less_than_or_equal_to: 0}
  validates :horizontal_recoil_first_10, numericality: {less_than_or_equal_to: 0}
  validates :vertical_recoil_first_20, numericality: {less_than_or_equal_to: 0}
  validates :horizontal_recoil_first_20, numericality: {less_than_or_equal_to: 0}
  validates :reload_time, numericality: {less_than_or_equal_to: 0}
 

  has_one_attached :image
  has_rich_text :content
end
