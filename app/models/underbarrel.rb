class Underbarrel < ApplicationRecord
  belongs_to :weapon
  has_many :underbarrel_recoils
  has_many :hip_fires, as: :hipfireable

end
