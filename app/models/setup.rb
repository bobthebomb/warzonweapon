class Setup < ApplicationRecord

    validates :name, presence: true
    validates :ads_time, numericality: {less_than_or_equal_to: 0}
    validates :hip_fire_cone, numericality: {less_than_or_equal_to: 0}
    validates :sprint_to_fire, numericality: {less_than_or_equal_to: 0}
    validates :bounce, numericality: {less_than_or_equal_to: 0}
    validates :vertical_recoil, numericality: {less_than_or_equal_to: 0}
    validates :horizontal_recoil, numericality: {less_than_or_equal_to: 0}
    validates :vertical_recoil_first_10, numericality: {less_than_or_equal_to: 0}
    validates :horizontal_recoil_first_10, numericality: {less_than_or_equal_to: 0}
    validates :vertical_recoil_first_20, numericality: {less_than_or_equal_to: 0}
    validates :horizontal_recoil_first_20, numericality: {less_than_or_equal_to: 0}
    validates :reload_time, numericality: {less_than_or_equal_to: 0}
    
end
