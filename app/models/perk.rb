class Perk < ApplicationRecord
  belongs_to :weapon
  has_many :hip_fires, as: :hipfireable

end
