class HipFire < ApplicationRecord
  belongs_to :hipfireable, polymorphic: true

  validates :xaxis, presence: true
  validates :yaxis, presence: true
end
