class Stock < ApplicationRecord
  belongs_to :weapon
  has_many :stock_recoils
  has_many :hip_fires, as: :hipfireable

end
