class Blog < ApplicationRecord
    extend FriendlyId
    friendly_id :title, use: :slugged
    validates :title, presence: true
    validates :description, presence: true
    validates :illustration, presence: true
    has_one_attached :illustration
    has_one_attached :image_1
    has_rich_text :body
    

end
