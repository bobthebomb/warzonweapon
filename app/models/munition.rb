class Munition < ApplicationRecord
  belongs_to :weapon
  has_many :munition_recoils
  has_many :hip_fires, as: :hipfireable

end
