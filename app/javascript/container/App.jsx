import React, { Component } from 'react';
import TypesList from './TypesList';
import WeaponsList from './WeaponsList';
import Axios from 'axios';
import WeaponDetail from '../component/WeaponDetail'
import {weaponMathStateUpdate} from '../component/weaponDetail/WeaponMath';
import AdminTab from '../component/AdminTab';


class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            weaponList: [],
            activeType: 0,
            activeWeapon: 0,
            weaponDetail: null,
            theMeta: [],
            weaponUpdated: {},
            meta: {}
        }
    }

    componentDidMount = () => {
        Axios
        .get( '/api/v1/setups.json')
        .then(response => {
            this.setState({
                theMeta: response.data.setups
            })
        })
        .catch(error => console.log(error))
        
        Axios
        .get('/api/v1/user.json')
        .then(response => {
            this.setState({
                meta: response.data.meta
            })        })
        .catch(error => console.log(error))

    }

    fetchWeaponsList = (typeId) => {
        Axios
            .get(`/api/v1/weapons.json?id=${typeId}`)
            .then(response => {
                const weaponsListing = response.data.weapons
                this.setState({
                    weaponList: [...weaponsListing],
                    activeType: typeId,
                    weaponDetail: null
                })
            })
            .catch(error => console.log(error))
    }

    fetchWeaponDetail = (weaponId) => {
        Axios
            .get(`/api/v1/weapons/${weaponId}.json`)
            .then(response => {

                const weaponDetail = response.data.weapon
                this.setState({

                    attachmentArray: [],        
                    maxAttachemnt: 0,
                    weaponDetail: { ...weaponDetail },
                    activeWeapon: weaponId,
                    weaponUpdated: { ...weaponDetail }
                })
            })
            .catch(error => console.log(error))
    }



    handleDuplicateAttachmentCount = (attachementStateName) => {
        if (!this.state[attachementStateName].hasOwnProperty('name')) {
            this.setState(current => ({
                maxAttachemnt: current.maxAttachemnt + 1
            }))
        }
    }


    checkNumberOfAttachment = () => {
        let nbOfOptions = 0
        for (let i = 0; i < this.state.attachmentArray.length; i++) {
            this.state.attachmentArray[i] ? nbOfOptions += 1 : nbOfOptions
        }
        return nbOfOptions
    }

    attachementNameArray = () => {
        const attachArray = []
        this.state.attachmentArray.map(attachement => {
            if(attachement.name !== undefined ){
                attachArray.push(attachement.name)
            }
        })
        return attachArray
    }

    handleToMeta = () => {
        console.log(this.state.weaponUpdated)
        const attachement_names = this.attachementNameArray()

        let newSetup = { 
            setup: { ...this.state.weaponUpdated, attachement_names }
        }
        delete newSetup.setup.id
        delete newSetup.setup.image
        delete newSetup.setup.content
        Axios
            .get('/api/v1/user.json')
            .then(response => {
                if(response.data.meta) {
                    Axios
                    .post('/api/v1/setups.json', newSetup)
                    .then(response => {
                    })
                    .catch(error => console.log(error))
                    alert("Added to meta")

                    
                }
            })  
            
            console.log(newSetup)
    }


    //De l'attachement Modal, je passe un objet attachement et un indice d'array
    //Si attachement est non null je check checkNumberOfAttachment pour voir si inf ou égale a 4, si c'est bon j'ajoute un l'objet a indice de array 
    // et ensuite je setstate
    // Si attachement est null c'est que j'ai cliqué sur supprimé ou rien choisir dans le modal donc je setState null.
    anotherAttachementArray = (attachement, indice) => {

        if (attachement) {
            const nbOfOptions = this.checkNumberOfAttachment()
            if (nbOfOptions <= 4) {
                const array = this.state.attachmentArray
                array[indice] = attachement
                this.setState({
                    attachmentArray: array
                })
            } else {
                window.confirm("Max number of Attachement ");
            }
        } else {
            const array = this.state.attachmentArray
                array[indice] = attachement
            this.setState({
                attachmentArray: array
            })
        }
        const updatedweapon = weaponMathStateUpdate(this.state.attachmentArray, this.state.weaponDetail)
        this.setState({
            weaponUpdated: updatedweapon
        })
    }

    render() {

        const adminTab = this.state.meta && this.state.meta.metaTabAdd ? <AdminTab addToMeta={this.handleToMeta} /> : null

        const weaponlist = this.state.weaponList.length > 0 ?
            <WeaponsList weaponList={this.state.weaponList}
                weaponDetail={this.fetchWeaponDetail}
                activeWeapon={this.state.activeWeapon}
            />
            :
            null

        const weaponDetail = this.state.weaponDetail ?
            <WeaponDetail weaponState={this.state}
                handleArray={this.anotherAttachementArray}
            />
            :
            null




        return (
            <div>
                {adminTab}
                <TypesList weaponsList={this.fetchWeaponsList} active={this.state.activeType}/>
                {weaponlist}
                {weaponDetail}

            </div>
        );
    }
}

export default App;