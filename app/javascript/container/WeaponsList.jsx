import Axios from 'axios';
import React, { Component } from 'react';
import Weapons from '../component/Weapons';
import "../component/Weapons.scss"

class WeaponsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

 

    render() {
        const theList = this.props.weaponList.map((weapon, key) => (
            <Weapons key={key} 
            weapon={weapon} 
            weaponDetail={this.props.weaponDetail}
            activeWeapon={this.props.activeWeapon}/>
        ))
        return (
            <div>
                <hr className="separator " />

                <div className="row align-items-center mt-4 weapon-card ">
                    {theList}
                </div>
            </div>
        );
    };
}

export default WeaponsList;