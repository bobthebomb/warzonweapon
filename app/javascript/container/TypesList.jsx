import Axios from 'axios';
import React, { Component } from 'react';
import Types from '../component/Types';
import "../component/Weapons.scss"

class TypesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            types: []
        }
    }

    componentDidMount = () => {
        Axios
            .get('/api/v1/types.json')
            .then(response => {
                const type = response.data.weapontype
                this.setState({
                    types: [...type]
                })
            })
            .catch(error => console.log(error))
    }

   

    render() {
        const weaponsTypes = this.state.types.map(type => (
            
            <Types 
            key={type.id} 
            type={type} 
            weaponsList={this.props.weaponsList}
            active={this.props.active}           
            />
        ))
        return (
            <div>

                <div className="row align-items-center ">
                    {weaponsTypes}                   
                </div>
            </div>
        );
    }
}

export default TypesList;

