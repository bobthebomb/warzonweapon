import React, { Component } from 'react';
import WeaponDamage from './weaponDetail/WeaponDamage';
import WeaponData from './weaponDetail/WeaponData';
import WeaponRecoil from './weaponDetail/WeaponRecoil'
import Axios from 'axios'
import WeaponImage from './WeaponImage';
import "./Weapons.scss"
import ReactHtmlParser from "react-html-parser";


class WeaponDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filler: true,

        }
    }




    render() {


        const title = this.props.weaponState.weaponDetail.name
        const content = this.props.weaponState.weaponDetail.content
        return (
            <div >

                <hr className="separator" />
                <div className="row">
                    <div className="col blog-part">

                        <h1>{title} </h1>

                    </div>
                </div>

                <div className="row">
                    <div className="col blog-part blog-body">

                    {ReactHtmlParser(content.body)}

                    </div>
                </div>


              


                <hr className="separator" />
                <div className="row">
                    <div className="col-lg-12">
                        <WeaponImage
                            weaponState={this.props.weaponState}

                            handleArray={this.props.handleArray}
                        />
                    </div>
                </div>
                <hr className="separator " />

                <div className="row">
                    <div className="col-lg-12 col-xs-12 text-center">
                        <WeaponDamage
                            weaponsData={this.props.weaponState}
                        />
                    </div>
                </div>
                <hr className="separator " />

                <div className="row">
                    <div className="col-lg-12 col-xs-12 text-center">
                        <WeaponData
                            weaponsData={this.props.weaponState} />
                    </div>
                </div>
                <hr className="separator " />


                <div className="row">
                    <div className="col-lg-12 col-xs-12 text-center">
                        <WeaponRecoil
                            weaponsData={this.props.weaponState} />
                    </div>
                </div>


            </div>



        );
    }
}

export default WeaponDetail;