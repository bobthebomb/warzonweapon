import React from 'react';
import {attachementMathRecoilBody, renderTheStatRecoilVsBody} from '../WeaponMath'
import RecoilTableHead from './RecoilTableHead';


const RecoilTwo = (props) => {
    const weaponName = props.recoilData.weaponsData.weaponDetail.name
    const weaponUpdated = props.recoilData.weaponsData.weaponUpdated
    const weaponBase = props.recoilData.weaponsData.weaponDetail
    const bodyHeight = 68
    const bodyWidth = 15.85
    const averageBody = (bodyHeight + bodyWidth) /2
    const bodyHeight5M = 150
    const bodywidth5M = 50
    const averageBody5M = (bodyHeight5M * bodywidth5M) /1000

    return (

        <div>
            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-hover">
                    <RecoilTableHead weaponName={weaponName} />
                    <caption>Basic COD skin at this range is 68px high and 15.85px width</caption>

                        <tbody>
                            
                            <tr>
                                <th scope="row">vertical recoil first 10 bullets</th>
                                {renderTheStatRecoilVsBody("vertical_recoil_first_10", weaponBase, weaponUpdated, "times of the  body Height",bodyHeight )} 
                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil first 10 bullets</th>
                    
                                {renderTheStatRecoilVsBody("horizontal_recoil_first_10", weaponBase, weaponUpdated, "times of the  body width",bodyWidth )} 

                            </tr>

                            <tr>
                                <th scope="row">vertical recoil first 20 bullets</th>
                                {renderTheStatRecoilVsBody("vertical_recoil_first_20", weaponBase, weaponUpdated, "times of the  body Height",bodyHeight )} 

                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil first 20 bullets</th>
                                {renderTheStatRecoilVsBody("horizontal_recoil_first_20", weaponBase, weaponUpdated, "times of the  body width",bodyWidth )} 

                            </tr>
                            <tr>
                                <th scope="row">Vertical recoil 30 bullets</th>
                                {renderTheStatRecoilVsBody("vertical_recoil", weaponBase, weaponUpdated, "times of the  body Height",bodyHeight )} 

                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil 30 bullets</th>
                                {renderTheStatRecoilVsBody("horizontal_recoil", weaponBase, weaponUpdated, "times of the  body width",bodyWidth )} 

                            </tr>

                            <tr>
                                <th scope="row">hip fire spread 15 bullets, 5m </th>
                                {renderTheStatRecoilVsBody("hip_fire_cone", weaponBase, weaponUpdated, "times of the body area", averageBody5M )} 

                            </tr>

                            <tr>
                                <th scope="row">Bounce</th>
                                {renderTheStatRecoilVsBody("bounce", weaponBase, weaponUpdated, "times of the  body range",averageBody )} 
                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>



        </div>




    );
}

export default RecoilTwo;