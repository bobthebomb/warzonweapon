import React, { Component } from 'react';

class RecoilTableHead extends Component {
    
    render() { 
        return ( 
            <thead>
                <tr>
                    <td>Weapon name is : </td>
                    <td>{this.props.weaponName}  </td>
                </tr>
            </thead>
         );
    }
}
 
export default RecoilTableHead;

