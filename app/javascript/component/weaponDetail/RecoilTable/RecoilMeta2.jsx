import React from 'react';


const RecoilMeta2 = (props) => {

    const weaponUpdated = props.recoilData.weaponsData.weaponUpdated
    const weaponMetaArray = props.recoilData.weaponsData.theMeta
    const metaId = props.idMeta
    const bodyHeight = 68
    const bodyWidth = 15.85
    const averageBody = (bodyHeight + bodyWidth) /2
    const bodyHeight5M = 150
    const bodywidth5M = 50
    const averageBody5M = (bodyHeight5M * bodywidth5M) /1000

    const renderTheStatMetaRatioBody = ( stat, metaWeapon, unit, bodypart)  => {
        
        function roundToTwo(num) {    
            return +(Math.round(num + "e+2")  + "e-2");
        }

        return metaWeapon.map( (uneWeapon, id) =>  {


                if(Number(uneWeapon.meta_id) === metaId){
                    const ratio = roundToTwo((Math.abs(uneWeapon[stat]) / bodypart))
                    
                    // const redOrGreen = weaponBase[stat] > uneWeapon[stat] ? "red" : "green"
                    // return (<td key={id}> <span className={redOrGreen}> {ratio}  </span> {unit} </td> )

            const redOrGreen = () => {
                if (weaponUpdated[stat] > uneWeapon[stat]) {
                     return "red"

                } else if (weaponUpdated[stat] < uneWeapon[stat]) {
                     return "green"

                } else {
                     return "orange"
                    
                }
            }

            if (Number(uneWeapon.meta_id) === metaId) {
                return (<td key={id}> <span className={redOrGreen()}> {ratio}  </span> {unit} </td> )
            }


                 }
            })
            
    }

    const renderTheStatMetaHead = ( stat, metaWeapon, unit)  => {
        return metaWeapon.map( (uneWeapon, id) =>  {
            if(Number(uneWeapon.meta_id) === metaId){
                  return (<th key={id} > {uneWeapon[stat]} {unit} </th> )
            }
            })
            
    }


    return (

        <div>

            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-hover">
                        <caption>Basic COD skin at this range is 68px high and 15.85px width META 2</caption>
                        <thead>
                            <tr>  
                                 {renderTheStatMetaHead("name",  weaponMetaArray, "" )} 
                            </tr>

                        </thead>
                        <tbody>                          
                            <tr>
                                {renderTheStatMetaRatioBody("vertical_recoil_first_10",  weaponMetaArray, "times of the body height", bodyHeight )} 
                            </tr>

                            <tr>                       
                                {renderTheStatMetaRatioBody("horizontal_recoil_first_10",  weaponMetaArray, "times of the body width", bodyWidth )} 
                            </tr>

                            <tr>                   
                                {renderTheStatMetaRatioBody("vertical_recoil_first_20",  weaponMetaArray, "times of the body height ",bodyHeight )} 
                            </tr>

                            <tr>                      
                                {renderTheStatMetaRatioBody("horizontal_recoil_first_20",  weaponMetaArray, "times of the body width" , bodyWidth)} 
                            </tr>

                            <tr>                        
                                {renderTheStatMetaRatioBody("vertical_recoil",  weaponMetaArray, "times of the body height" , bodyHeight)} 
                            </tr>

                            <tr>                             
                                {renderTheStatMetaRatioBody("horizontal_recoil",  weaponMetaArray, "times of the body width", bodyWidth )} 
                            </tr>

                            <tr>                             
                                {renderTheStatMetaRatioBody("hip_fire_cone",  weaponMetaArray, "times of the body area", averageBody5M )} 
                            </tr>
                         
                            <tr>               
                                {renderTheStatMetaRatioBody("bounce",  weaponMetaArray, "times of the body average", averageBody )} 
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>




    );
}

export default RecoilMeta2;