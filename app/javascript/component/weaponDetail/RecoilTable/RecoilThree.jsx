import React from 'react';
import {renderTheStatRecoilVsBody} from '../WeaponMath'
import RecoilTableHead from './RecoilTableHead';


const RecoilThree = (props) => {

    const weaponName = props.recoilData.weaponsData.weaponDetail.name

    const weaponBase = props.recoilData.weaponsData.weaponDetail
    const weaponUpdated = props.recoilData.weaponsData.weaponUpdated
            // Calcul % de recul sur 18m basé sur la tete horizontal et vertical
    const headHeight = 12.67
    const headWidth = 10
    const headAverage = (headHeight + headWidth) /2
    const bodyHeight5M = 150
    const bodywidth5M = 50
    const averageBody5M = (bodyHeight5M * bodywidth5M) /1000

    return (

        <div>
            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-hover">
                    <caption>Basic COD head loc at this range is 12.67px high and 10px width</caption>

                        <RecoilTableHead weaponName={weaponName} />
                        <tbody>
                            
                            <tr>
                                <th scope="row">vertical recoil first 10 bullets</th>
                                {renderTheStatRecoilVsBody("vertical_recoil_first_10", weaponBase, weaponUpdated, "times of the  head Height",headHeight )} 
                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil first 10 bullets</th>
                    
                                {renderTheStatRecoilVsBody("horizontal_recoil_first_10", weaponBase, weaponUpdated, "times of the  head width",headWidth )} 

                            </tr>

                            <tr>
                                <th scope="row">vertical recoil first 20 bullets</th>
                                {renderTheStatRecoilVsBody("vertical_recoil_first_20", weaponBase, weaponUpdated, "times of the  head Height",headHeight )} 

                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil first 20 bullets</th>
                                {renderTheStatRecoilVsBody("horizontal_recoil_first_20", weaponBase, weaponUpdated, "times of the  body width",headWidth )} 

                            </tr>
                            <tr>
                                <th scope="row">Vertical recoil 30 bullets</th>
                                {renderTheStatRecoilVsBody("vertical_recoil", weaponBase, weaponUpdated, "times of the  head Height",headHeight )} 

                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil 30 bullets</th>
                                {renderTheStatRecoilVsBody("horizontal_recoil", weaponBase, weaponUpdated, "times of the  head width",headWidth )} 

                            </tr>

                            <tr>
                                <th scope="row">hip fire spread 15 bullets, 5m </th>
                                {renderTheStatRecoilVsBody("hip_fire_cone", weaponBase, weaponUpdated, "times of the head area", averageBody5M )} 

                            </tr>

                            <tr>
                                <th scope="row">Bounce</th>
                                {renderTheStatRecoilVsBody("bounce", weaponBase, weaponUpdated, "times of the  head range",headAverage )} 
                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>



        </div>




    );
}

export default RecoilThree;