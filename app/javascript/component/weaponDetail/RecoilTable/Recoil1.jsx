import React from 'react';
import { renderTheStat} from '../WeaponMath'
import RecoilTableHead from './RecoilTableHead';


const Recoil1 = (props) => {

    const weaponUpdated = props.recoilData.weaponsData.weaponUpdated
    const weaponBase = props.recoilData.weaponsData.weaponDetail
    const weaponName = props.recoilData.weaponsData.weaponDetail.name
    return (

        <div>

            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-hover">
                        <caption>Basic COD skin at this range is 68px high and 15.85px width</caption>
                        <RecoilTableHead weaponName={weaponName} />
                        <tbody>                          

                            <tr>
                                <th scope="row">vertical recoil first 10 bullets</th>
                                {renderTheStat("vertical_recoil_first_10", weaponBase, weaponUpdated, "pixels" )} 
                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil first 10 bullets</th>
                                {renderTheStat("horizontal_recoil_first_10", weaponBase, weaponUpdated, "pixels" )}
                            </tr>

                            <tr>
                                <th scope="row">vertical recoil first 20 bullets</th>
                                {renderTheStat("vertical_recoil_first_20", weaponBase, weaponUpdated, "pixels" )}
                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil first 20 bullets</th>
                                {renderTheStat("horizontal_recoil_first_20", weaponBase, weaponUpdated, "pixels" )}
                            </tr>

                            <tr>
                                <th scope="row">Vertical recoil 30 bullets</th>
                                {renderTheStat("vertical_recoil", weaponBase, weaponUpdated, "pixels" )}
                            </tr>

                            <tr>
                                <th scope="row">Horizontal recoil 30 bullets</th>
                                {renderTheStat("horizontal_recoil", weaponBase, weaponUpdated, "pixels" )}

                            </tr>

                            <tr>
                                <th scope="row">hip fire spread 15 bullets, 5m </th>
                                {renderTheStat("hip_fire_cone", weaponBase, weaponUpdated, " k.px²" )}

                            </tr>

                            <tr>
                                <th scope="row">Bounce</th>
                                {renderTheStat("bounce", weaponBase, weaponUpdated, "pixels" )}
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>




    );
}

export default Recoil1;