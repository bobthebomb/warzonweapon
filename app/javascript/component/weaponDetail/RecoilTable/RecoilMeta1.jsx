import React from 'react';


const RecoilMeta1 = (props) => {

    const weaponUpdated = props.recoilData.weaponsData.weaponUpdated
    const weaponMetaArray = props.recoilData.weaponsData.theMeta
    const weaponBase = props.recoilData.weaponsData.weaponDetail
    const metaId = props.idMeta

    const renderTheStatMeta = (stat, metaWeapon, unit) => {

        return metaWeapon.map((uneWeapon, id) => {

            function roundToTwo(num) {    
                return +(Math.round(num + "e+2")  + "e-2");
            }

            const redOrGreen = () => {
                if (weaponUpdated[stat] > uneWeapon[stat]) {
                    return "red"

                } else if (weaponUpdated[stat] < uneWeapon[stat]) {
                    return "green"

                } else {
                    return "orange"

                }
            }
            if (Number(uneWeapon.meta_id) === metaId) {
                return (<td key={id}> <span className={redOrGreen()}> {roundToTwo(Math.abs(uneWeapon[stat]))}  </span> {unit} </td>)
            }
        })

    }

    const renderTheStatMetaHead = (stat, metaWeapon, unit) => {
        return metaWeapon.map((uneWeapon, id) => {
            if (Number(uneWeapon.meta_id) === metaId) {
                return (<th key={id} > {uneWeapon[stat]} {unit} </th>)
            }
        })

    }

    return (

        <div>

            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-hover">
                        <caption>Basic COD skin at this range is 68px high and 15.85px width</caption>
                        <thead>
                            <tr>
                                {renderTheStatMetaHead("name", weaponMetaArray, "")}
                            </tr>

                        </thead>
                        <tbody>

                            <tr>
                                {renderTheStatMeta("vertical_recoil_first_10", weaponMetaArray, "pixels")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("horizontal_recoil_first_10", weaponMetaArray, "pixels")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("vertical_recoil_first_20", weaponMetaArray, "pixels")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("horizontal_recoil_first_20", weaponMetaArray, "pixels")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("vertical_recoil", weaponMetaArray, "pixels")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("horizontal_recoil", weaponMetaArray, "pixels")}
                            </tr>


                            <tr>
                                {renderTheStatMeta("hip_fire_cone", weaponMetaArray, "k.px²")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("bounce", weaponMetaArray, "pixels")}
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>



        </div>




    );
}

export default RecoilMeta1;