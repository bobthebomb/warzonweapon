import React, { Component } from 'react';
import WeaponData1 from './DataTable/WeaponData1';
import WeaponDataMeta from './DataTable/WeaponDataMeta';

class WeaponData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infoLength: 4,
            infoIndice: 2
        }
    }


    HandlePreviousMeta = () => {
        if (this.state.infoIndice <= 1) {
            this.setState({
                infoIndice: this.state.infoLength,
            })
        } else {
            this.setState(currentState => ({
                infoIndice: currentState.infoIndice - 1,

            }))
        }

    }

    HandlenextMeta = () => {
        if (this.state.infoIndice >= this.state.infoLength) {
            this.setState({
                infoIndice: 1
            })
        } else {
            this.setState(currentState => ({
                infoIndice: currentState.infoIndice + 1
            }))
        }
    }





    render() {

        const titleArray = [
            "non existent",
            "SMGs",
            "ARs",
            "BR",
            "LMG"
        ]


        return (
            <div className="card  dpsMetaChart text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">

                                <div className="col">
                                    <h2>General Stats</h2>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col">
                                    <WeaponData1 weaponStats={this.props} />
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="row">
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        onClick={this.HandlePreviousMeta}  >
                                        <i className="fas fa-arrow-left"></i>
                                    </button>
                                </div>

                                <div className="col">
                                    <h2>{titleArray[this.state.infoIndice]}</h2>
                                </div>
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        onClick={this.HandlenextMeta}>
                                        <i className="fas fa-arrow-right"></i>
                                    </button>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col ">
                                    <WeaponDataMeta weaponStats={this.props} metaId={this.state.infoIndice} />

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        );
    }
}

export default WeaponData;