import React from 'react';
import { renderTheStat } from "../WeaponMath"

const WeaponData1 = (props) => {


    const weaponUpdated = props.weaponStats.weaponsData.weaponUpdated
    const weaponBase = props.weaponStats.weaponsData.weaponDetail
    const burst = props.weaponStats.weaponsData.weaponDetail.burst > 0 ? <tr>
    <th scope="row">Burst</th>
    {renderTheStat("burst", weaponBase, weaponUpdated, "B/m")}

        </tr> : null

    return (

        <div>
            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-striped table-hover">
                        <thead>
                            <tr>
                                <td > Weapon Name: </td>
                                <td> {weaponUpdated.name} </td>
                            </tr>
                        </thead>

                        <tbody>
                     


                            <tr>
                                <th scope="row"  >Bullet velocity</th>
                                {renderTheStat("bullet_velocity", weaponBase, weaponUpdated, "ms")}
                            </tr>
                            <tr>
                                <th scope="row"  >Ads speed</th>
                                {renderTheStat("ads_time", weaponBase, weaponUpdated, "ms")}
                            </tr>

                            <tr>
                                <th scope="row">Sprint to fire</th>
                                {renderTheStat("sprint_to_fire", weaponBase, weaponUpdated, "ms")}

                            </tr>
                            <tr>
                                <th scope="row">RPM</th>
                                {renderTheStat("rpm", weaponBase, weaponUpdated, "b/m")}

                            </tr>

                            {burst}

                            <tr>
                                <th scope="row">Mag size</th>
                                {renderTheStat("magazine", weaponBase, weaponUpdated, "ammo")}

                            </tr>

                            <tr>
                                <th scope="row">Reload time</th>
                                {renderTheStat("reload_time", weaponBase, weaponUpdated, "s")}
                            </tr>

                            <tr>
                                <th scope="row">Move</th>
                                {renderTheStat("movement", weaponBase, weaponUpdated, "m/s")}
                            </tr>
                            <tr>
                                <th scope="row">Ads movement</th>
                                {renderTheStat("ads_movement", weaponBase, weaponUpdated, "m/s")}
                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>



        </div>

    );

}

export default WeaponData1;