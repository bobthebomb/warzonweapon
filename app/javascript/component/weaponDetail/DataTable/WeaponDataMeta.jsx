import React from 'react';
// import { renderTheStatMeta } from "../WeaponMath"

const WeaponDataMeta = (props) => {


    const weaponUpdated = props.weaponStats.weaponsData.weaponUpdated
    const weaponMeta = props.weaponStats.weaponsData.theMeta
    const metaId = props.metaId

    const renderTheStatMeta = (stat, metaWeapon, unit) => {

        return metaWeapon.map((uneWeapon, id) => {
            // const redOrGreen = weaponUpdated[stat] > uneWeapon[stat] ? "red" : "green"

            const redOrGreen = () => {
                if (weaponUpdated[stat] > uneWeapon[stat]) {
                    return "red"

                } else if (weaponUpdated[stat] < uneWeapon[stat]) {
                    return "green"

                } else {
                    return "orange"

                }
            }

            function roundToTwo(num) {
                return +(Math.round(num + "e+2") + "e-2");
            }

            if (Number(uneWeapon.meta_id) === metaId) {
                return (<td key={id}> <span className={redOrGreen()}> {roundToTwo(Math.abs(uneWeapon[stat]))}  </span> {unit} </td>)
            }
        })

    }

    const renderTheBurst = () => {


       

        if (props.weaponStats.weaponsData.weaponDetail.burst > 0) {

            return weaponMeta.map((uneWeapon, id) => {

                const redOrGreen = () => {
                    if (weaponUpdated.burst > uneWeapon.burst) {
                        return "red"
        
                    } else if (weaponUpdated.burst < uneWeapon.burst) {
                        return "green"
        
                    } else {
                        return "orange"
                    }
                }

                if (Number(uneWeapon.meta_id) === metaId) {
                    return uneWeapon.burst > 0 ?   <td className={redOrGreen()} key={id}> {uneWeapon.burst} </td> : <td key={id}> -- </td>
                }
            }
            )
        } else {
            return null
        }
    }

    const renderTheStatMetaHead = (stat, metaWeapon, unit) => {
        return metaWeapon.map((uneWeapon, id) => {
            if (Number(uneWeapon.meta_id) === metaId) {
                return (<th key={id} > {uneWeapon[stat]} {unit} </th>)
            }
        })

    }



    return (

        <div>
            <div className="card bg-dark text-white" style={{ width: "100%" }}>
                <div className="card-body">
                    <table className="table table-dark table-striped table-hover">
                        <thead>
                            <tr>
                                {renderTheStatMetaHead("name", weaponMeta, "")}
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                {renderTheStatMeta("bullet_velocity", weaponMeta, "m/s")}
                            </tr>
                            <tr>

                                {renderTheStatMeta("ads_time", weaponMeta, "ms")}
                            </tr>

                            <tr>

                                {renderTheStatMeta("sprint_to_fire", weaponMeta, "ms")}

                            </tr>
                            <tr>

                                {renderTheStatMeta("rpm", weaponMeta, "b/m")}

                            </tr>

                            <tr>
                            {renderTheBurst()}
                            </tr>
                         

                            <tr>
                                {renderTheStatMeta("magazine", weaponMeta, "b")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("reload_time", weaponMeta, "s")}
                            </tr>

                            <tr>
                                {renderTheStatMeta("movement", weaponMeta, "m/s")}
                            </tr>
                            <tr>
                                {renderTheStatMeta("ads_movement", weaponMeta, "m/s")}
                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    );


}

export default WeaponDataMeta;