import { Fragment } from "react";
import React, { Component } from 'react';

class ModalMeta extends Component {

    handleClick = () => {
        this.props.toggleModal()
    }


    displayA = () => {
        if (this.props.metaWeapon.hasOwnProperty('attachement_names') && this.props.metaWeapon.attachement_names.length > 0){
            const listAttachement = this.props.metaWeapon.attachement_names.map((attachment, i) => {
                return <li className="list-group-item list-group-item-dark" key={i}>  {attachment}  </li>
            }

            )
            return listAttachement
        } else {
            return <li> No Attachement </li>
        }
    }

    render() {


        return (

            <Fragment>
                <div className="Modal-mini">
                    <div className=" row" >
                        <div className="col">
                            <div >
                                <h2>{this.props.metaWeapon.name}</h2>
                                <ul className="list-group">

                                    {this.displayA()}
                                </ul>
                                <br></br>
                                <button className="btn btn-outline-info" onClick={this.handleClick}>
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default ModalMeta;