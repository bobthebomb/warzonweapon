import React, { Fragment, PureComponent } from 'react';
import {
  ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, BarChart, Bar,
} from 'recharts';
import ModalMeta from './ModalMeta'
import { chartGenRange } from './ChartHelper'
import  ReactTooltip  from 'react-tooltip'

class ChartDpsMeta extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      infoIndice: 1,
      meta_id:0,
      show_modal: false,
      metaWeapon: {},
      weaponBuilt: {
        name: '',
        attachement_names: [],
        attachementArray: []
      }
    }
  }

  componentDidMount = () => {
    ReactTooltip.rebuild();

    const nameArray = []
    
    this.props.data.weaponsData.attachmentArray.map( unAttachment => {
      if(unAttachment !== null && unAttachment.hasOwnProperty('name')) {
        nameArray.push(unAttachment.name)
      }
    })
    this.setState({
      weaponBuilt: { ...this.state.weaponBuilt, 
         name: this.props.data.weaponsData.weaponDetail.name, 
          attachement_names: nameArray,
          attachementArray: this.props.data.weaponsData.attachmentArray }
    })
  }

  componentDidUpdate = (prevProps) => {

    if(prevProps.data.weaponsData.weaponUpdated !== this.props.data.weaponsData.weaponUpdated) {
      console.log("c'est OUI")
      this.activeWeaponAttachementNames()
    }

  }


  activeWeaponAttachementNames = () => {
    const nameArray = []
    this.props.data.weaponsData.attachmentArray.map( unAttachment => {
      if(unAttachment !== null &&  unAttachment.hasOwnProperty('name')) {

        nameArray.push(unAttachment.name)
      }
    })
    this.setState({
      weaponBuilt: { ...this.state.weaponBuilt,  name: this.props.data.weaponsData.weaponDetail.name,  attachement_names: nameArray}
    })
  }

 


  openModal = (e) => {
    console.log(e)
    this.setState({
      show_modal: true,
      metaWeapon: e.payload.metaWeapon
    })
  }

  closeModal =() => {
    this.setState({
      show_modal: false
    })
  }
 
 
  handleIndiceChange = (indice) => {
    this.handleTitleChange(indice)
    this.setState({
      infoIndice: indice
    })
  }


handleMetaChange = (indice) => {
  this.setState({
    meta_id : indice
  })
}

handleTitleChange = (indice) => {

  const titleArray = [
    " Head ",
    " Chest ",
    " Stomach ",
    " Limbs "
  ]

  const myTitle = titleArray[indice]
  this.props.title(myTitle)
}

  render() {

  

 
    const modal = this.state.show_modal ?  <ModalMeta metaWeapon={this.state.metaWeapon} toggleModal={this.closeModal}/> : null
    const weaponUpdated = this.props.data.weaponsData.weaponUpdated
    const laWeapon = this.props.data.weaponsData.weaponDetail

    //je fabrique le param que je vais passer dans legend de rechart pour passer le nom des attachements.
    //uniquement pour les meta au final ! 
    

    const colorArray = [
      "lime", "red", "forestGreen", "SandyBrown", "cyan", "DeepSkyBlue", "DarkRed", "DarkViolet", "DeepPink"
    ]



    const locaArray = [
      "head_dmg_range_",
      "chest_dmg_range_",
      "stomach_dmg_range_",
      "limbs_dmg_range_"
    ]


    const genScatter = this.props.data.weaponsData.theMeta.map((oneWeapon, id) => 
       this.state.meta_id == oneWeapon.meta_id ?
        <Scatter  key={id} legendType="line"       
        name={`${oneWeapon.name} meta`}
        metaWeapon={oneWeapon}
        data={chartGenRange(oneWeapon, locaArray[this.state.infoIndice])}
        fill={colorArray[id]} line={{strokeWidth: 2}} shape="circle"/> : null
      
    )

    const CustomTooltip = ({ active, payload }) => {
      if (active) {
        return (
          <div className="card border-dark mb-3" >
            <h4 className="card-header text-dark">{payload[0].payload.nameWeapon}</h4>
            <div className="card-body text-dark">
            
            <p className="card-text">{payload[0].name}: {payload[0].value} {payload[0].unit}</p>
            <p className="card-text">{payload[1].name}: {payload[1].value}</p>
            </div>
          </div>
        );
      }
      return null;
    };


    return (
      <Fragment >
 
        <div className="row">
          <div className="col">
            <button className="btn btn-outline-info btn-block" 
            data-tip="Dps on Headshot"
              onClick={() => this.handleIndiceChange(0)}>Head </button>
          </div>
          <div className="col">
            <button className="btn btn-outline-info btn-block"
                                    data-tip="Dps on chestshot"
              onClick={() => this.handleIndiceChange(1)}>Chest </button>
          </div>
          <div className="col">
            <button className="btn btn-outline-info btn-block"
                                    data-tip="Dps on Stomachshot"

              onClick={() => this.handleIndiceChange(2)}>Stomach </button>
          </div>
          <div className="col">
            <button className="btn btn-outline-info btn-block"
                                    data-tip="Dps on Limbs Shot"

              onClick={() => this.handleIndiceChange(3)}>Limbs </button>
          </div>
        </div>
          <ResponsiveContainer width="100%" height={350}>
          <ScatterChart
            margin={{
              top:20, right: 0, bottom: 5, left: 20,
            }}
          >
            <CartesianGrid strokeDasharray="1 1" />
            <XAxis type="number" tickCount="7" dataKey="x" name="Distance" unit=" meters" domain={[0, dataMax => (60)]} />
            <YAxis  type="number"  dataKey="y" name="DPS" unit=" dps" domain={[dataMin => (Math.floor(dataMin) - 20 ), dataMax => (Math.ceil(dataMax) + 20 )]} />
            <Tooltip cursor={{ strokeDasharray: '3 3' }} dataKey={"nameWeapon"} content={<CustomTooltip />} />
            <Legend classname="click-me" onClick={(a) => this.openModal(a)} />
            <Scatter legendType="line" name={`${laWeapon.name} you built`} 
            data={chartGenRange(weaponUpdated, locaArray[this.state.infoIndice])} 
            metaWeapon={this.state.weaponBuilt}
            fill="DeepPink" line={{strokeWidth: 3}} shape="circle"/>           
            {genScatter}
          </ScatterChart>
        </ResponsiveContainer>
        
            {/* option pour le Y axis si l'auto marche pas bien */}
        {/* <YAxis  ticks={[100,200,300,400,500,600]} type="number" dataKey="y" name="DPS" unit=" dps" domain={[0, 600]} /> */}
        {modal}
        <div className="row">
          <div className="col">
              <button className="btn btn-outline-info btn-block"
                                      data-tip="Clear the chart"

                onClick={() => this.handleMetaChange(0)}>Off</button>
                
            </div>
            <div className="col">
              <button className="btn btn-outline-info btn-block"
                data-tip="Compare your weapon with SMG meta"

                onClick={() => this.handleMetaChange(1)}>SMG</button>
            </div>
            <div className="col">
              <button className="btn btn-outline-info btn-block"
                              data-tip="Compare your weapon with AR meta"

                onClick={() => this.handleMetaChange(2)}>AR</button>
            </div>
            <div className="col">
              <button className="btn btn-outline-info btn-block"
                              data-tip="Compare your weapon with battle rifle Meta"

                onClick={() => this.handleMetaChange(3)}>BR</button>
            </div>

            <div className="col">
            <button className="btn btn-outline-info btn-block"
              data-tip="Compare your weapon with LMG Meta"

              onClick={() => this.handleMetaChange(4)}>LMG</button>
          </div>
        </div>



      </Fragment>
    );
  }
}

export default ChartDpsMeta;