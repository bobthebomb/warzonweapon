


export const chartGenRange = (attachements, laLoc) => {
    const rpm = attachements.rpm
    return [ { x: 0, y: dpsMaker(attachements[`${laLoc}1`], rpm), nameWeapon: attachements.name  }, // commence tjs a 0 et vise le Y de la premiere stat
        { x: attachements.range_1, y: dpsMaker(attachements[`${laLoc}1`], rpm), nameWeapon: attachements.name },  // 1er stat normale x et Y
        { x: attachements.range_1 + 0.01 , y: dpsMaker(attachements[`${laLoc}2`], rpm), nameWeapon: attachements.name}, // x avance de 0.01 et y de la deuxieme stat.
        { x: attachements.range_2, y: dpsMaker(attachements[`${laLoc}2`], rpm), nameWeapon: attachements.name}, // deuxiement stat x et y
        attachements.range_3_used && { x: attachements.range_2 + 0.01, y: dpsMaker(attachements[`${laLoc}3`], rpm), nameWeapon: attachements.name }, // x avance de 0.01 et y de la troisemen stat 
        attachements.range_3_used && { x: attachements.range_3, y: dpsMaker(attachements[`${laLoc}3`], rpm), nameWeapon: attachements.name}, //Troisiemen stat normale  
        attachements.range_4_used &&{ x: attachements.range_3 +0.01, y: dpsMaker(attachements[`${laLoc}4`], rpm), nameWeapon: attachements.name }, 
        attachements.range_4_used &&{ x: attachements.range_4, y: dpsMaker(attachements[`${laLoc}4`], rpm), nameWeapon: attachements.name}
       ];
}

const dpsMaker = (dmg ,rpm) => {
    return (dmg * rpm) / 60
}


export const chartGenRangeTtk = ( attachements, laLoc, useAdsSpeed) => {
    const ads = useAdsSpeed ? Math.abs(attachements.ads_time / 1000) : 0
    const burst = attachements.burst

    const rpm = attachements.rpm
    return [ { x: 0, y: ttkMaker(attachements[`${laLoc}1`], rpm, ads ,burst) , nameWeapon: attachements.name  }, // commence tjs a 0 et vise le Y de la premiere stat
    { x: attachements.range_1, y: ttkMaker(attachements[`${laLoc}1`], rpm, ads, burst), nameWeapon: attachements.name },  // 1er stat normale x et Y
    { x: attachements.range_1 + 0.01 , y: ttkMaker(attachements[`${laLoc}2`], rpm, ads, burst), nameWeapon: attachements.name}, // x avance de 0.01 et y de la deuxieme stat.
    { x: attachements.range_2, y: ttkMaker(attachements[`${laLoc}2`], rpm, ads, burst), nameWeapon: attachements.name}, // deuxiement stat x et y
    attachements.range_3_used && { x: attachements.range_2 + 0.01, y: ttkMaker(attachements[`${laLoc}3`], rpm, ads, burst), nameWeapon: attachements.name }, // x avance de 0.01 et y de la troisemen stat 
    attachements.range_3_used && { x: attachements.range_3, y: ttkMaker(attachements[`${laLoc}3`], rpm, ads, burst), nameWeapon: attachements.name}, //Troisiemen stat normale  
    attachements.range_4_used &&{ x: attachements.range_3 +0.01, y: ttkMaker(attachements[`${laLoc}4`], rpm, ads, burst), nameWeapon: attachements.name }, 
    attachements.range_4_used &&{ x: attachements.range_4, y: ttkMaker(attachements[`${laLoc}4`], rpm, ads, burst), nameWeapon: attachements.name}
       
       ];
}


const ttkMaker = (dmg, rpm, ads, burst) => {
    if(burst > 0) {
    const numberOfBullet = Math.ceil(250 / dmg)
    const burstToKill = Math.ceil(numberOfBullet/3)
    const burstPerSecond = burst / 60
    return ((burstToKill / burstPerSecond) + ads ).toFixed(2)

    } else {
    const numberOfBullet = Math.ceil(250 / dmg)
    const bulletPerSecond = rpm / 60
    return ((numberOfBullet / bulletPerSecond) + ads ).toFixed(2)
    }
    
}