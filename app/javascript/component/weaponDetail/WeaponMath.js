

import React from 'react';

export const weaponTtkPrime = (dmg, rpm, burst) => {
    if(burst > 0 ) {
    const nbOfBullet = Math.ceil(250 / dmg)
    const nbOfBurstToKill = Math.ceil(nbOfBullet/3)
    const burstPerSecond = burst/60
    return (nbOfBurstToKill / burstPerSecond).toFixed(2)

    } else {

    const nbOfBullet = Math.ceil(250 / dmg)
    const bulletPerSecond = rpm / 60
    return (nbOfBullet / bulletPerSecond).toFixed(2)
    }
    
}

export const weaponStkPrime = (dmg, burst) => {
    if(burst > 0 ) {
        const nbOfBullet = Math.ceil(250 / dmg)
        const nbOfBurstToKill = Math.ceil(nbOfBullet/3)
        return nbOfBurstToKill

    } else {

        const nbOfBullet = Math.ceil(250 / dmg)
        return nbOfBullet
    }
    
}

export const weaponDpsPrime = (dmg, rpm) => {
    const dps = dmg * rpm / 60
    return dps
}

export const weaponTtk = (props) => {
    const nbOfBullet = Math.ceil(250 / props.chest_dmg_range_1)
    const bulletPerSecond = props.rpm / 60
    return (nbOfBullet / bulletPerSecond).toFixed(2)
}

export const weaponTtk2 = (props) => {
    const nbOfBullet = Math.ceil(250 / props.chest_dmg_range_2)
    const bulletPerSecond = props.rpm / 60
    return (nbOfBullet / bulletPerSecond).toFixed(2)
}

export const weaponTtk3 = (props) => {
    const nbOfBullet = Math.ceil(250 / props.chest_dmg_range_3)
    const bulletPerSecond = props.rpm / 60
    return (nbOfBullet / bulletPerSecond).toFixed(2)
}

export const rangeMax = (props) => {
    const rangeTxt = !props.range_3_used ? "unlim" : ` ${props.range_2}m`
    return rangeTxt
}

export const rangeMax3 = (props) => {
    const rangeTxt = !props.range_4_used ? "unlim" : `${props.range_3}m`
    return rangeTxt
}

export const mathData = (props, mod) => {
    let laVar = props.rpm
    if (mod.canonChosen.hasOwnProperty("name")) {
        laVar - mod.canonChosen.rpm
    }
    if (mod.ammoChosen.hasOwnProperty("name")) {
        laVar - mod.ammoChosen.rpm
    }
    return laVar
}

const leLoop = (laStat, optionArray) => {
    let totalStatWithAttachment = 0
    const array = optionArray
    for (let i = 0; i < array.length; i++) {
        if (array[i]) {
            array[i][laStat] ? totalStatWithAttachment += array[i][laStat] : 1
        }

        // return la sum des attachments

    }
    return totalStatWithAttachment
}

const redOrGreen = (baseState, laStat, displayedStat) => {

    const baseStateInNumber = Number(baseState[laStat])
    const displayedStatInNumber = Number(displayedStat)



    if (baseStateInNumber != displayedStatInNumber) {
        return baseStateInNumber <= displayedStatInNumber ? "green" : "red"

    } else {
        return ''
    }
}

export const attachmentMath = (laStat, baseStat, optionArray, label) => {
    //attachement array commence vide (true), ensuite chaque case sera null(false) ou objet (true)
    //la stat c'est le param (string) qui call fonction dans le render
    //baseStat : c'est la this.props des stats de base de l'arme (ca vient du fetch)
    //option array c'est le array d'attachement que je constitue en APP et que je send en bas.
    // loop or render ce serait top

    let displayedStat
    if (optionArray.length > 0) {
        const attachementTotal = leLoop(laStat, optionArray)
        displayedStat = Number(attachementTotal) + Number(baseStat[laStat])
        if (Number(displayedStat) === Number(baseStat[laStat])) {
            return <td ><span className={redOrGreen(baseStat, laStat, displayedStat)}>
                {Math.abs(displayedStat)}</span> {label} </td>

        } else {
            return <td > {Math.abs(baseStat[laStat])}
                <i className="fas fa-arrow-right"></i>
                <span className={redOrGreen(baseStat, laStat, displayedStat)}>{Math.abs(displayedStat)}</span> {label} </td>

        }
    } else {
        displayedStat = baseStat[laStat]
        return <td ><span className={redOrGreen(baseStat, laStat, displayedStat)}>
            {Math.abs(displayedStat)}</span> {label} </td>

    }
}


const RegleDeTroisBody = (laStat, laZoneVisee) => {
    const bodyPixel18hauteur = laZoneVisee //body pixel d'une cible a 18 metre
    const calculedHauteur = (laStat * 100) / bodyPixel18hauteur
    return calculedHauteur.toFixed(2)
}

export const attachementMathRecoilBody = (laStat, baseStat, optionArray, label, laZoneVisee) => {
    let displayedStat
    if (optionArray.length > 0) {
        const attachementTotal = leLoop(laStat, optionArray)
        displayedStat = Number(attachementTotal) + Number(baseStat[laStat])
        if (Number(displayedStat) === Number(baseStat[laStat])) {
            return <td ><span className={redOrGreen(baseStat, laStat, displayedStat)}>
                {Math.abs(RegleDeTroisBody(baseStat[laStat], laZoneVisee))}</span> {label} </td>

        } else {
            return <td > {Math.abs(RegleDeTroisBody(baseStat[laStat], laZoneVisee))}
                <i className="fas fa-arrow-right"></i> <span className={redOrGreen(baseStat, laStat, displayedStat)}>
                    {Math.abs(RegleDeTroisBody(displayedStat, laZoneVisee))}</span> {label} </td>

        }
    } else {
        displayedStat = baseStat[laStat]
        return <td ><span className={redOrGreen(baseStat, laStat, displayedStat)}>
            {Math.abs(RegleDeTroisBody(baseStat[laStat], laZoneVisee))}</span> {label} </td>

    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////

const MathSurLesAttachment = (key, value, weaponDetail, subtotal) => {
    if(value !== 0 ){
    const lePourcentage = value * 100 / weaponDetail[key]
    return subtotal * (lePourcentage/100)
    } else {
        return 0 
    }
}

export const weaponMathStateUpdate = (optionArray, weaponDetail) => {
    let fakeObject = {...weaponDetail}
    optionArray.map(option => {
        if (option !== null) {
            for (const [key, value] of Object.entries(option)) {
                if(typeof value === "number"){
                    //MathSurLesAttachment calcul le % que represente l'attachement applique le % sur la stat actuelle et ainsi de suite
                    
                    fakeObject[key] = MathSurLesAttachment(key, value, weaponDetail, fakeObject[key]) + Number(fakeObject[key])
                }
            }
        }
    })

    return fakeObject
}


export const renderTheStat = (laStat,base, modified, unit) => {

    function roundToTwo(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }

    const origin = Number(base[laStat])
    const made = roundToTwo(Number(modified[laStat]))
    if(origin === made){
    return <td> {Math.abs(origin)} {unit}</td>
    } else if(origin > made) {
        return <td>  {Math.abs(origin)} <i className="fas fa-arrow-right"></i> <span  className="red"> {Math.abs(made)} </span> {unit}</td>
    } else {
        return <td> {Math.abs(origin)} <i className="fas fa-arrow-right"></i>  <span className="green">  {Math.abs(made)} </span> {unit}</td>
    }
}

const BodypartMath = (recoil, bodySize) => {

    function roundToTwo(num) {    
        return +(Math.round(num + "e+2")  + "e-2");
    }

     let ratioRecoil = (recoil  ) / (bodySize )
     ratioRecoil = roundToTwo(ratioRecoil)
     return ratioRecoil
}
// comme render the stat function mais avec une regle de 3 pour les % sur body/head size 
export const renderTheStatRecoilVsBody = (laStat,base, modified, unit, bodyPart) => {

    

    const origin = Number(base[laStat])
    const made = Number(modified[laStat])
    if(origin === made){
    return <td> {BodypartMath(Math.abs(origin), bodyPart)} {unit}</td>
    } else if(origin > made) {
        return <td>  {BodypartMath(Math.abs(origin), bodyPart)} <i className="fas fa-arrow-right"></i> <span  className="red"> {BodypartMath(Math.abs(made), bodyPart)} </span> {unit}</td>
    } else {
        return <td> {BodypartMath(Math.abs(origin), bodyPart)} <i className="fas fa-arrow-right"></i>  <span className="green">  {BodypartMath(Math.abs(made), bodyPart)} </span> {unit}</td>
    }
}


