import React, { Component } from 'react';
import LocalisationDamage3Range from "./DamageTable/LocalisationDamage3Range";
import LocalisationTtk3Range from "./DamageTable/LocalisationTtk3Range";
import LocalisationStk3Range from "./DamageTable/LocalisationStk3Range";
import LocalisationDps3Range from "./DamageTable/LocalisationDps3Range";
import TableHead from './DamageTable/TableHead';
import ChartDpsMeta from './charts/ChartDpsMeta';
import ChartTtkMeta from './charts/ChartTtkMeta';
import ReactTooltip from 'react-tooltip';

class WeaponDamage extends Component {
 
    constructor(props) {
        super(props);
        this.state = {
            infoIndice: 0,
            infoLength: 3 ,// la longeur du array weaponInfo c'est pour la boucle
            chartIndice: 0,
            chartLength: 1,
            chartTitle: ["Compare Dps", "Compare Ttk" ],
            chartTitleLoca: 'chest'
        // bon j'ai une double boucle pour les table et les chart..mais j'ai que 
        //2 charts..un button serait plus simple et moins brittle
        } 
    }

    

    handlePrevious = () => {
        if (this.state.infoIndice <= 0) {
            this.setState({
                infoIndice: this.state.infoLength,
            })
        } else {
            this.setState(currentState => ({
                infoIndice: currentState.infoIndice - 1,

            }))
        }
    }

    handleNext = () => {
        if (this.state.infoIndice >= this.state.infoLength) {
            this.setState({
                infoIndice: 0
            })
        } else {
            this.setState( currentState => ({
                infoIndice: currentState.infoIndice + 1 
            }))
        }
      
    }


    handlePreviousChart = () => {
        if (this.state.chartIndice <= 0) {
            this.setState({
                chartIndice: this.state.chartLength,
                chartTitleLoca: 'chest'
            })
        } else {
            this.setState(currentState => ({
                chartIndice: currentState.chartIndice - 1,
                chartTitleLoca: 'chest'
            }))
        }
    }

    handleNextChart = () => {
        if (this.state.chartIndice >= this.state.chartLength) {
            this.setState({
                chartIndice: 0,
                chartTitleLoca: 'chest'
            })
        } else {
            this.setState( currentState => ({
                chartIndice: currentState.chartIndice + 1,
                chartTitleLoca: 'chest'
            }))
        }
      
    }

    handleChartTitle = (title) => {
        this.setState({
            chartTitleLoca: title
        })
    }


    render() {

        const burstTtk = this.props.weaponsData.weaponDetail.burst > 0 ? "(Burst)" : null
        const burstStk = this.props.weaponsData.weaponDetail.burst > 0 ? "Burst" : "shots"


        const chartInfo = [
            <ChartDpsMeta data={this.props} title={this.handleChartTitle}/>,
            <ChartTtkMeta data={this.props} title={this.handleChartTitle}/>
        ]

        const weaponInfo = [
            <LocalisationDamage3Range weaponsDamage={this.props} />,
            <LocalisationDps3Range weaponsDamage={this.props} /> ,
            <LocalisationTtk3Range weaponsDamage={this.props} />,
            <LocalisationStk3Range weaponsDamage={this.props} /> 
        ]

        const titleArray = [
                <p> Damage for each range </p>,
                <p> Damage per second for each range </p>,
                <p> Time to kill for each range {burstTtk}</p>,
                <p> Number of {burstStk} to kill by range</p>
        ]

        const tooltipArrow = this.state.chartIndice === 0 ?  "Time to kill chart" :  "Damage per second chart"
           
        
      
        return (
            <div className="card  dpsMetaChart text-white" style={{ width: "100%" }}>
                <ReactTooltip border={true} arrowColor="green"/>

                <div className="card-body">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                       onClick={this.handlePrevious}><i className="fas fa-arrow-left"></i> </button>
                                </div>
                                <div className="col">
                                    <h2> Damage </h2>
                                </div>
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        onClick={this.handleNext}> <i className="fas fa-arrow-right"></i></button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    {titleArray[this.state.infoIndice]}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <table className="table table-dark table-hover table-striped">
                                        <TableHead tableHead={this.props} />
                                        {weaponInfo[this.state.infoIndice]}
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                        <div className="row">
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        data-tip={tooltipArrow}
                                       onClick={this.handlePreviousChart}><i className="fas fa-arrow-left"></i> </button>
                                </div>
                                <div className="col-xs-10">
                                    <h2> {this.state.chartTitle[this.state.chartIndice]} {this.state.chartTitleLoca}  </h2>
                                </div>
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                    data-tip={tooltipArrow}
                                        onClick={this.handleNextChart}> <i className="fas fa-arrow-right"></i></button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col ">
                                    {chartInfo[this.state.chartIndice]}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        );
    }
}

export default WeaponDamage;