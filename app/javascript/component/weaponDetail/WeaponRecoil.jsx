import React, { Component } from 'react';
import Recoil1 from './RecoilTable/Recoil1';
import RecoilTwo from './RecoilTable/RecoilTwo';
import RecoilThree from './RecoilTable/RecoilThree';
import RecoilMeta1 from './RecoilTable/RecoilMeta1';
import RecoilMeta2 from './RecoilTable/RecoilMeta2';
import RecoilMeta3 from './RecoilTable/RecoilMeta3';

class WeaponRecoil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infoIndice: 0,
            infoLength: 2, // la longeur du array weaponInfo c'est pour la boucle
            idMeta: 2
        }


    }



    handlePrevious = () => {
        if (this.state.infoIndice <= 0) {
            this.setState({
                infoIndice: this.state.infoLength
            })
        } else {
            this.setState(currentState => ({
                infoIndice: currentState.infoIndice - 1
            }))
        }
    }

    handleNext = () => {
        if (this.state.infoIndice >= this.state.infoLength) {
            this.setState({
                infoIndice: 0
            })
        } else {
            this.setState(currentState => ({
                infoIndice: currentState.infoIndice + 1
            }))
        }

    }

    handleIdMeta = (metaId) => {
        this.setState({
            idMeta: metaId
        })
    }


    render() {

        const recoilArray = [
            <Recoil1 recoilData={this.props} />,
            <RecoilTwo recoilData={this.props} /> ,
            <RecoilThree recoilData={this.props} />
        ]

        const metaRecoilArray = [
            <RecoilMeta1 recoilData={this.props} idMeta={this.state.idMeta} />,
            <RecoilMeta2 recoilData={this.props} idMeta={this.state.idMeta} /> ,
            <RecoilMeta3 recoilData={this.props} idMeta={this.state.idMeta} />
        ]

        const titleArray = [
            <p> ReCoil Pixel measurement at 18 meters </p>,
            <p> Recoil % of target body at 18 meters</p>,
            <p> Recoil % of target Head at 18 meters</p>
        ]



        return (
            <div className="card  dpsMetaChart text-white" style={{ width: "100%" }}>
                <div className="card-body">

                    <div className="row">
                        <div className="col-md-6">

                            <div className="row">
                                <div className="col">
                                    <button
                                        className="btn btn-outline-info"
                                        onClick={this.handlePrevious}
                                    ><i className="fas fa-arrow-left"></i></button>
                                </div>


                                <div className="col">
                                    <h2> Recoil Data</h2>
                                </div>

                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        onClick={this.handleNext}> <i className="fas fa-arrow-right"></i></button>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col">
                                     {titleArray[this.state.infoIndice]}                                
                                </div>
                            </div>


                            <div className="row">
                                <div className="col">
                                    {recoilArray[this.state.infoIndice]}
                                </div>
                            </div>
                        </div>


                        <div className="col-md-6">
                            <div className="row">

                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        onClick={this.handlePrevious}
                                    ><i className="fas fa-arrow-left"></i></button>
                                </div>

                                <div className="col">
                                    <h2>Recoil Meta</h2>
                                </div>
                                <div className="col">
                                    <button className="btn btn-outline-info"
                                        onClick={this.handleNext}
                                    > <i className="fas fa-arrow-right"></i></button>
                                </div>
                            </div>


                            <div className="row">
                                <div className="col">                                
                                 <button className="btn btn-outline-info btn-block need-a-top-margin"
                                 onClick={() => this.handleIdMeta(1)} >Smg</button>
                                 </div>
                                 <div className="col">                                
                                 <button className="btn btn-outline-info btn-block need-a-top-margin"
                                 onClick={() => this.handleIdMeta(2)}>Ar</button>
                                 </div>
                                 <div className="col">                                
                                 <button className="btn btn-outline-info btn-block need-a-top-margin"
                                 onClick={() => this.handleIdMeta(3)}>BR</button>
                                 </div>
                                 <div className="col">  
                                 <button className="btn btn-outline-info btn-block need-a-top-margin"
                                 onClick={() => this.handleIdMeta(4)}>LMG</button>
                                 </div>
                            </div>

                            <div className="row">
                                <div className="col">
                                    {metaRecoilArray[this.state.infoIndice]}
                                </div>
                            </div>

                        </div>
                    </div>

                </div >
            </div >

        );
    }
}

export default WeaponRecoil;