import React, { Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalGrip = (props) => {

    const handleClick = (grip) => {
        props.closeModal()
        // props.handleChoose(grip, "gripChosen")
        props.handleArray(grip, 7)

    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("gripChosen")
        props.handleArray(null, 7)

    }
  

    const mapping = props.data.map((grip, id) => {
        return (
            <tr key={id} >
                <th > {grip.name} </th>
                <td><button onClick={() => handleClick(grip)} className="btn btn-outline-info btn-block"> equip </button></td>
               
                <td className={redOrGreen(grip.ads_time)}>{Math.abs(grip.ads_time)} ms</td>
                <td className={redOrGreen(grip.sprint_to_fire)}>{Math.abs(grip.sprint_to_fire)} ms</td>
                <td className={redOrGreen(grip.vertical_recoil)}>{Math.abs(grip.vertical_recoil)} px</td>
                <td className={redOrGreen(grip.horizontal_recoil)}>{Math.abs(grip.horizontal_recoil)} px</td>
                <td className={redOrGreen(grip.bounce)}>{Math.abs(grip.bounce)} px</td>
                <td className={redOrGreen(grip.hip_fire_cone)}>{Math.abs(grip.hip_fire_cone)} k.px²</td>
                <td className="modal-td">{grip.note}</td>

            </tr>
        )

    })



    return (    

<Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> Grip attachment choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>
                                <th scope="col">Ads Time Modifier</th>
                                <th scope="col">Sprint to Fire Modifier</th>                           
                                <th scope="col">Vertical Recoil modifier</th>
                                <th scope="col">Horizontal Recoil modifier</th>
                                <th scope="col">Recoil Bounce Modifier</th>
                                <th scope="col">Hip Fire Modifier</th>
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>

     );
}
 
export default ModalGrip;