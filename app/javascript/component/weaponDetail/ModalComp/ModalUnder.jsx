import React, { Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalUnder = (props) => {

    const handleClick = (under) => {
        props.closeModal()
        // props.handleChoose(under, "underbarrelChosen")
        props.handleArray(under, 5)

    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("underbarrelChosen")
        props.handleArray(null, 5)
    }
    

    const mapping = props.data.map((underbarrel, id) => {
        return (
            <tr key={id} >
                <th > {underbarrel.name} </th>
                <td><button onClick={() => handleClick(underbarrel)} className="btn btn-outline-info btn-block"> equip </button></td>
               
                <td className={redOrGreen(underbarrel.ads_time)}>{Math.abs(underbarrel.ads_time)} ms</td>
                
                <td className={redOrGreen(underbarrel.vertical_recoil_first_10)}>{Math.abs(underbarrel.vertical_recoil_first_10)} px</td>
                <td className={redOrGreen(underbarrel.horizontal_recoil_first_10)}>{Math.abs(underbarrel.horizontal_recoil_first_10)} px</td>
                <td className={redOrGreen(underbarrel.vertical_recoil_first_20)}>{Math.abs(underbarrel.vertical_recoil_first_20)} px</td>
                <td className={redOrGreen(underbarrel.horizontal_recoil_first_20)}>{Math.abs(underbarrel.horizontal_recoil_first_20)} px</td>
                <td className={redOrGreen(underbarrel.vertical_recoil)}>{Math.abs(underbarrel.vertical_recoil)} px</td>
                <td className={redOrGreen(underbarrel.horizontal_recoil)}>{Math.abs(underbarrel.horizontal_recoil)} px</td>

                <td className={redOrGreen(underbarrel.bounce)}>{Math.abs(underbarrel.bounce)} px</td>
                <td className={redOrGreen(underbarrel.hip_fire_cone)}>{Math.abs(underbarrel.hip_fire_cone)} k.px²</td>
                <td className={redOrGreen(underbarrel.movement)}>{Math.abs(underbarrel.movement)}  m/s</td>
                <td className="modal-td">{underbarrel.note}</td>

            </tr>
        )

    })



    return (    

<Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> UnderBarrel attachment choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>
                                <th scope="col">Ads Time Modifier</th>
                             
                                <th scope="col">first 10 bullets Vertical Recoil modifier </th>
                                    <th scope="col">first 10 bullets Horizontal Recoil modifier</th>
                                    <th scope="col">first 20 bullets Vertical Recoil modifier</th>
                                    <th scope="col">first 20 bullets Horizontal Recoil modifier</th>
                                    <th scope="col">total Vertical Recoil modifier</th>
                                    <th scope="col">total  Horizontal Recoil modifier</th>

                                <th scope="col">Recoil Bounce Modifier</th>
                                <th scope="col">Hip Fire Modifier</th>
                                <th scope="col">Move Speed Modifier</th>
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>

     );
}
 
export default ModalUnder;