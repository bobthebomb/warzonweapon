import React, { Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalStock = (props) => {

    const handleClick = (stock) => {
        props.closeModal()
        // props.handleChoose(stock, "stockChosen")
        props.handleArray(stock, 4)

        
    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("stockChosen")
        props.handleArray(null, 4)

    }
    

    const mapping = props.data.map((stock, id) => {
        return (
            <tr key={id} >
                <th > {stock.name} </th>
                <td><button onClick={() => handleClick(stock)} className="btn btn-outline-info btn-block"> equip </button></td>              
                <td className={redOrGreen(stock.ads_time)}>{Math.abs(stock.ads_time)} ms</td>
                <td className={redOrGreen(stock.vertical_recoil)}>{Math.abs(stock.vertical_recoil)} px</td>
                <td className={redOrGreen(stock.horizontal_recoil)}>{Math.abs(stock.horizontal_recoil)} px</td>
                <td className={redOrGreen(stock.bounce)}>{Math.abs(stock.bounce)} px</td>
                <td className={redOrGreen(stock.hip_fire_cone)}>{Math.abs(stock.hip_fire_cone)} k.px²</td>
                <td className={redOrGreen(stock.movement)}>{Math.abs(stock.movement)}  m/s</td>
                <td className="modal-td">{stock.note}</td>

            </tr>
        )


    })
 // Schemas a ajouté a stock : les modificateurs de reculs (tous), source, ads, note, movement, range 

    return (
        <Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> Stock attachment choice</h1>
            </div>

        </div>

        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>                              
                                <th scope="col">Ads Time Modifier</th>
                                <th scope="col">Vertical Recoil modifier</th>
                                <th scope="col">Horizontal Recoil modifier</th>
                                <th scope="col">Recoil Bounce Modifier</th>
                                <th scope="col">Hip Fire Modifier</th>
                                <th scope="col">Move Speed Modifier</th>
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>
    );
}

export default ModalStock;