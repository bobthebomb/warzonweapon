import React, { Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalMunition = (props) => {

    const handleClick = (munition) => {
        props.closeModal()
        // props.handleChoose(munition, "munitionChosen")
        props.handleArray(munition, 6)
    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("munitionChosen")
        props.handleArray(null, 6)

    }
   

    const mapping = props.data.map((munition, id) => {
        return (
            <tr key={id} >
                <th > {munition.name} </th>
                <td><button onClick={() => handleClick(munition)} className="btn btn-outline-info btn-block"> equip </button></td>
                <td className={redOrGreen(munition.magazine)}>{Math.abs(munition.magazine)} ammo</td>
                <td className={redOrGreen(munition.reload_time)}>{Math.abs(munition.reload_time)} sec</td>
                <td className={redOrGreen(munition.ads_time)}>{Math.abs(munition.ads_time)} ms</td>              
                <td className={redOrGreen(munition.movement)}>{Math.abs(munition.movement)}  m/s</td>
                <td className="modal-td">{munition.note}</td>

            </tr>
        )



    })



    return (    


        <Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> munition attachment choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>
                                <th scope="col">Bonus ammo number</th>
                                <th scope="col">Reload Speed Modifier</th>
                                <th scope="col">Ads Time Modifier</th>  
                                <th scope="col">Move Speed Modifier</th>
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>

     );
}
 
export default ModalMunition;