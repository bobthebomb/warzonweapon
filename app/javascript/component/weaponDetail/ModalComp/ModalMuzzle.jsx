import React, { Fragment } from 'react';
import "./Modal.scss"
import {redOrGreen} from './ModalHelper'

const ModalMuzzle = (props) => {

    const handleClick = (muzzle) => {
        props.closeModal()
        props.handleArray(muzzle, 0)
    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("muzzleChosen")
        props.handleArray(null, 0)

    }


   
    const mapping = props.data.map((muzzle, id) => {
        return (
            <tr key={id} >
                <th > {muzzle.name} </th>
                <td><button onClick={() => handleClick(muzzle)} className="btn btn-outline-info btn-block"> equip </button></td>
                <td className={redOrGreen(muzzle.range_1)} >{Math.abs(muzzle.range_1)} meters</td>
                <td className={redOrGreen(muzzle.bullet_velocity)}>{Math.abs(muzzle.bullet_velocity)} m/s</td>
                <td className={redOrGreen(muzzle.ads_time)}>{Math.abs(muzzle.ads_time)} ms</td>
               
                <td className={redOrGreen(muzzle.vertical_recoil_first_10)}>{Math.abs(muzzle.vertical_recoil_first_10)} px</td>
                <td className={redOrGreen(muzzle.horizontal_recoil_first_10)}>{Math.abs(muzzle.horizontal_recoil_first_10)} px</td>
                <td className={redOrGreen(muzzle.vertical_recoil_first_20)}>{Math.abs(muzzle.vertical_recoil_first_20)} px</td>
                <td className={redOrGreen(muzzle.horizontal_recoil_first_20)}>{Math.abs(muzzle.horizontal_recoil_first_20)} px</td>
                <td className={redOrGreen(muzzle.vertical_recoil)}>{Math.abs(muzzle.vertical_recoil)} px</td>
                <td className={redOrGreen(muzzle.horizontal_recoil)}>{Math.abs(muzzle.horizontal_recoil)} px</td>

                <td className={redOrGreen(muzzle.bounce)}>{Math.abs(muzzle.bounce)} px</td>
                <td className="modal-td">{muzzle.note}</td>
            </tr>
        )


    })


    // Schemas a ajouté a canon : les modificateurs de reculs (tous), source, ads, note, movement, range 

    return (

        <Fragment>
            <div className="Modal">
            <div className=" row" >
                <div className="col">
                    <h1 className="modal-title"> Muzzle attachment choice</h1>
                </div>

            </div>


            <div className="row" >
                <div className="col">
                    <div className="table-responsive">
                        <table className="table table-dark table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">#</th>
                                    <th scope="col">Range Modifier</th>
                                    <th scope="col">Bullet Velocity Modifier</th>
                                    <th scope="col">Ads Time Modifier</th>
                                   
                                    <th scope="col">first 10 bullets Vertical Recoil modifier </th>
                                    <th scope="col">first 10 bullets Horizontal Recoil modifier</th>
                                    <th scope="col">first 20 bullets Vertical Recoil modifier</th>
                                    <th scope="col">first 20 bullets Horizontal Recoil modifier</th>
                                    <th scope="col">total Vertical Recoil modifier</th>
                                    <th scope="col">total  Horizontal Recoil modifier</th>

                                    <th scope="col">Recoil Bounce Modifier</th>
                                    <th scope="col">Notes</th>
                                </tr>

                            </thead>

                            <tbody>
                                {mapping}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>






                <button
                    onClick={() => handleNotClick()}
                    className="btn btn-outline-info btn-block">

                    Choose nothing or remove attachement
                </button>      
            </div>
            <div className="overlay">

</div>

        </Fragment>
    );
}

export default ModalMuzzle;