import React, { Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalOptic = (props) => {

    const handleClick = (optic) => {
        props.closeModal()
        // props.handleChoose(optic, "opticChosen")
        props.handleArray(optic, 3)

    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("opticChosen")
        props.handleArray(null, 3)
    }
    

    const mapping = props.data.map((optic, id) => {
        return (
            <tr key={id} >
                <th > {optic.name} </th>
                <td><button onClick={() => handleClick(optic)} className="btn btn-outline-info btn-block"> equip </button></td>
                <td className={redOrGreen(optic.ads_time)}>{Math.abs(optic.ads_time)} ms</td>
                <td className={redOrGreen(optic.vertical_recoil)}>{Math.abs(optic.vertical_recoil)} px</td>
                <td className={redOrGreen(optic.horizontal_recoil)}>{Math.abs(optic.horizontal_recoil)} px</td>
                <td className={redOrGreen(optic.bounce)}>{Math.abs(optic.bounce)} px</td>
                <td className="modal-td">{optic.note}</td>

            </tr>
        )


    })



    return (    

<Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> Optic attachment choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>                 
                                <th scope="col">Ads Time Modifier</th>
                                <th scope="col">Vertical Recoil modifier</th>
                                <th scope="col">Horizontal Recoil modifier</th>
                                <th scope="col">Recoil Bounce Modifier</th>                  
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>

     );
}
 
export default ModalOptic;