import React, {  Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalLaser = (props) => {

    const handleClick = (laser) => {
        props.closeModal()
        // props.handleChoose(laser, "laserChosen")
        props.handleArray(laser, 2)

    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("laserChosen")
        props.handleArray(null, 2)

    }
   
    const mapping = props.data.map((laser, id) => {
        return (
            <tr key={id} >
                <th > {laser.name} </th>
                <td><button onClick={() => handleClick(laser)} className="btn btn-outline-info btn-block"> equip </button></td>
                <td className={redOrGreen(laser.ads_time)}>{Math.abs(laser.ads_time)} ms</td>
                <td className={redOrGreen(laser.sprint_to_fire)}>{Math.abs(laser.sprint_to_fire)} ms</td>
                <td className={redOrGreen(laser.hip_fire_cone)}>{Math.abs(laser.hip_fire_cone)} k.px²</td>
                <td className="modal-td">{laser.note}</td>

            </tr>
        )

    })
 // Schemas a ajouté a laser : les modificateurs de reculs (tous), source, ads, note, movement, range 

    return (
        <Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> Laser attachment choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>   
                                <th scope="col">Ads Time Modifier</th>
                                <th scope="col">Sprint to fire modifier</th>
                                <th scope="col">Hip Fire Modifier</th>                           
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>
    );
}

export default ModalLaser;