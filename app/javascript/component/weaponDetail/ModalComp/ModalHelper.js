export const redOrGreen = (bonus) => {
    if(bonus > 0) {
        return "green"
    } else if (bonus < 0) {
        return "red"
    } else {
        return "orange"
    }
}
