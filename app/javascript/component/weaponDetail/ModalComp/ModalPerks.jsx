import React, { Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalPerk = (props) => {

    const handleClick = (perk) => {
        props.closeModal()
        // props.handleChoose(perk, "perkChosen")
        props.handleArray(perk, 8)

    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("perkChosen")
        props.handleArray(null, 8)

    }
  

    const mapping = props.data.map((perk, id) => {
        return (
            <tr key={id} >
                <th > {perk.name} </th>
                <td><button onClick={() => handleClick(perk)} className="btn btn-outline-info btn-block"> equip </button></td>
                <td className={redOrGreen(perk.effect)} >{perk.effect}</td>
            </tr>
        )


    })



    return (    

<Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> Perks choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>              
                                <th scope="col">Perks effect</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>
     );
}
 
export default ModalPerk;