import React, {  Fragment } from 'react';
import {redOrGreen} from './ModalHelper'

const ModalCanon = (props) => {

    const handleClick = (canon) => {
        props.closeModal()
        // props.handleChoose(canon, "canonChosen")
        props.handleArray(canon, 1)
    }

    const handleNotClick = () => {
        props.closeModal()
        // props.handleNotChoose("canonChosen")
        props.handleArray( null, 1)
    }

   

    const mapping = props.data.map((canon, id) => {
        return (
            <tr key={id} >
                <th > {canon.name} </th>
                <td><button onClick={() => handleClick(canon)} className="btn btn-outline-info btn-block"> equip </button></td>
                <td className={redOrGreen(canon.range_1)} >{Math.abs(canon.range_1)} meters</td>
                <td className={redOrGreen(canon.bullet_velocity)}>{Math.abs(canon.bullet_velocity)} m/s</td>
                <td className={redOrGreen(canon.ads_time)}>{Math.abs(canon.ads_time)} ms</td>              
                <td className={redOrGreen(canon.vertical_recoil_first_10)}>{Math.abs(canon.vertical_recoil_first_10)} px</td>
                <td className={redOrGreen(canon.horizontal_recoil_first_10)}>{Math.abs(canon.horizontal_recoil_first_10)} px</td>
                <td className={redOrGreen(canon.vertical_recoil_first_20)}>{Math.abs(canon.vertical_recoil_first_20)} px</td>
                <td className={redOrGreen(canon.horizontal_recoil_first_20)}>{Math.abs(canon.horizontal_recoil_first_20)} px</td>
                <td className={redOrGreen(canon.vertical_recoil)}>{Math.abs(canon.vertical_recoil)} px</td>
                <td className={redOrGreen(canon.horizontal_recoil)}>{Math.abs(canon.horizontal_recoil)} px</td>
                <td className={redOrGreen(canon.bounce)}>{Math.abs(canon.bounce)} px</td>
                <td className={redOrGreen(canon.hip_fire_cone)}>{Math.abs(canon.hip_fire_cone)} k.px²</td>
                <td className={redOrGreen(canon.movement)}>{Math.abs(canon.movement)} m/s</td>
                <td className="modal-td">{canon.note}</td>

            </tr>
        )



    })
   // Schemas a ajouté a canon : les modificateurs de reculs (tous), source, ads, note, movement, range 

   return (

    <Fragment>
        <div className="Modal">
        <div className=" row" >
            <div className="col">
                <h1 className="modal-title"> Barrel attachment choice</h1>
            </div>

        </div>


        <div className="row" >
            <div className="col">
                <div className="table-responsive">
                    <table className="table table-dark table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">#</th>
                                <th scope="col">Range Modifier</th>
                                <th scope="col">Bullet Velocity Modifier</th>
                                <th scope="col">Ads Time Modifier</th>  

                                <th scope="col">first 10 bullets Vertical Recoil modifier </th>
                                <th scope="col">first 10 bullets Horizontal Recoil modifier</th>
                                <th scope="col">first 20 bullets Vertical Recoil modifier</th>
                                <th scope="col">first 20 bullets Horizontal Recoil modifier</th>
                                <th scope="col">total Vertical Recoil modifier</th>
                                <th scope="col">total  Horizontal Recoil modifier</th>

                                <th scope="col">Recoil Bounce Modifier</th>
                                <th scope="col">Hip Fire Modifier</th>
                                <th scope="col">Move Speed Modifier</th>
                                <th scope="col">Notes</th>
                            </tr>

                        </thead>

                        <tbody>
                            {mapping}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>






            <button
                onClick={() => handleNotClick()}
                className="btn btn-outline-info btn-block">

                Choose nothing or remove attachement
            </button>      
        </div>
        <div className="overlay">

</div>

    </Fragment>
);
}

export default ModalCanon;