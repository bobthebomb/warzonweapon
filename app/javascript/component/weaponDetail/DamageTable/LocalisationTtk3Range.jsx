import React from 'react';
import { weaponTtkPrime} from "../WeaponMath"



const LocalisationTtk3Range = (props) => {

    const { rpm, range_1, burst,
        range_3_used, range_4_used,
        head_dmg_range_1, head_dmg_range_2, head_dmg_range_3, head_dmg_range_4,
        chest_dmg_range_1, chest_dmg_range_2, chest_dmg_range_3, chest_dmg_range_4,
        stomach_dmg_range_1, stomach_dmg_range_2, stomach_dmg_range_3, stomach_dmg_range_4,
        limbs_dmg_range_1, limbs_dmg_range_2, limbs_dmg_range_3, limbs_dmg_range_4 } = props.weaponsDamage.weaponsData.weaponUpdated



    
    return (


            <tbody>
                <tr>
                    <th>Head</th>
                    <td>{weaponTtkPrime(head_dmg_range_1, rpm, burst)} S</td>
                    <td>{weaponTtkPrime(head_dmg_range_2, rpm, burst)} S</td>
                    {range_3_used && <td>{weaponTtkPrime(head_dmg_range_3, rpm, burst)} S</td>}
                    {range_4_used && <td>{weaponTtkPrime(head_dmg_range_4, rpm, burst)} S</td>}
                </tr>

                <tr>
                    <th>Chest</th>
                    <td>{weaponTtkPrime(chest_dmg_range_1, rpm, burst)} S</td>
                    <td>{weaponTtkPrime(chest_dmg_range_2, rpm, burst)} S</td>
                    {range_3_used && <td>{weaponTtkPrime(chest_dmg_range_3, rpm, burst)} S</td>}
                    {range_4_used && <td>{weaponTtkPrime(chest_dmg_range_4, rpm, burst)} S</td>}
                </tr>

                <tr>
                    <th>Stomach</th>
                    <td>{weaponTtkPrime(stomach_dmg_range_1, rpm, burst)} S</td>
                    <td>{weaponTtkPrime(stomach_dmg_range_2, rpm, burst)} S</td>
                    {range_3_used && <td>{weaponTtkPrime(stomach_dmg_range_3, rpm, burst)} S</td>}
                    {range_4_used && <td>{weaponTtkPrime(stomach_dmg_range_4, rpm, burst)} S</td>}
                </tr>
                <tr>
                    <th>Limbs</th>
                    <td>{weaponTtkPrime(limbs_dmg_range_1, rpm, burst)} S</td>
                    <td>{weaponTtkPrime(limbs_dmg_range_2, rpm, burst)} S</td>
                    {range_3_used && <td>{weaponTtkPrime(limbs_dmg_range_3, rpm, burst)} S</td>}
                    {range_4_used && <td>{weaponTtkPrime(limbs_dmg_range_4, rpm, burst)} S</td>}
                </tr>
            </tbody>
   



    );

}

export default LocalisationTtk3Range;