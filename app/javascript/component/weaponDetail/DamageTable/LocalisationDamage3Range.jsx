import React, { Component } from 'react';
import TableHead from './TableHead';

class LocalisationDamage3Range extends Component {
    constructor(props) {
        super(props)

    }



    render() {
        const { range_1,
            range_3_used, range_4_used,
            head_dmg_range_1, head_dmg_range_2, head_dmg_range_3, head_dmg_range_4,
            chest_dmg_range_1, chest_dmg_range_2, chest_dmg_range_3, chest_dmg_range_4,
            stomach_dmg_range_1, stomach_dmg_range_2, stomach_dmg_range_3, stomach_dmg_range_4,
            limbs_dmg_range_1, limbs_dmg_range_2, limbs_dmg_range_3, limbs_dmg_range_4 } = this.props.weaponsDamage.weaponsData.weaponUpdated

        // voir la collone range 3 si range 2 n'est pas infiny, ok? donc si range_3_used . pareil pour range4

       
        return (

                <tbody>
                    <tr>
                        <th>Head</th>
                        <td>{head_dmg_range_1}</td>
                        <td>{head_dmg_range_2}</td>
                        {range_3_used && <td>{head_dmg_range_3}</td>}
                        {range_4_used && <td>{head_dmg_range_4}</td>}
                    </tr>

                    <tr>
                        <th>Chest</th>
                        <td>{chest_dmg_range_1}</td>
                        <td>{chest_dmg_range_2}</td>
                        {range_3_used && <td>{chest_dmg_range_3}</td>}
                        {range_4_used && <td>{chest_dmg_range_4}</td>}
                    </tr>

                    <tr>
                        <th>Stomach</th>
                        <td>{stomach_dmg_range_1}</td>
                        <td>{stomach_dmg_range_2}</td>
                        {range_3_used && <td>{stomach_dmg_range_3}</td>}
                        {range_4_used && <td>{stomach_dmg_range_4}</td>}
                    </tr>
                    <tr>
                        <th>Limbs</th>
                        <td>{limbs_dmg_range_1}</td>
                        <td>{limbs_dmg_range_2}</td>
                        {range_3_used && <td>{limbs_dmg_range_3}</td>}
                        {range_4_used && <td>{limbs_dmg_range_4}</td>}
                    </tr>
                </tbody>



        );
    }
}

export default LocalisationDamage3Range;