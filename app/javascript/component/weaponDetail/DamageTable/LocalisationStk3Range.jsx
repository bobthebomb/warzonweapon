import React  from 'react';
import  {weaponStkPrime } from "../WeaponMath"



const LocalisationStk3Range = (props) =>  {
    
        const { range_1, burst,
            range_3_used, range_4_used,
            head_dmg_range_1, head_dmg_range_2, head_dmg_range_3, head_dmg_range_4,
            chest_dmg_range_1, chest_dmg_range_2, chest_dmg_range_3, chest_dmg_range_4,
            stomach_dmg_range_1, stomach_dmg_range_2, stomach_dmg_range_3, stomach_dmg_range_4,
            limbs_dmg_range_1, limbs_dmg_range_2, limbs_dmg_range_3, limbs_dmg_range_4  } = props.weaponsDamage.weaponsData.weaponUpdated

   

         
        return (

            
                
                <tbody>
                    <tr>
                        <th>Head</th>
                        <td>{weaponStkPrime(head_dmg_range_1, burst)} B</td>
                        <td>{weaponStkPrime(head_dmg_range_2, burst)} B</td>
                        {range_3_used && <td>{weaponStkPrime(head_dmg_range_3, burst)} B</td>}
                        {range_4_used && <td>{weaponStkPrime(head_dmg_range_4, burst)} B</td>}
                    </tr>

                    <tr>
                        <th>Chest</th>
                        <td>{weaponStkPrime(chest_dmg_range_1, burst)} B</td>
                        <td>{weaponStkPrime(chest_dmg_range_2, burst)} B</td>
                        {range_3_used && <td>{weaponStkPrime(chest_dmg_range_3, burst)} B</td>}
                        {range_4_used && <td>{weaponStkPrime(chest_dmg_range_4, burst)} B</td>}
                    </tr>

                    <tr>
                        <th>Stomach</th>
                        <td>{weaponStkPrime(stomach_dmg_range_1, burst)} B</td>
                        <td>{weaponStkPrime(stomach_dmg_range_2, burst)} B</td>
                        {range_3_used && <td>{weaponStkPrime(stomach_dmg_range_3, burst)} B</td>}
                        {range_4_used && <td>{weaponStkPrime(stomach_dmg_range_4, burst)} B</td>}
                    </tr>
                    <tr>
                        <th>Limbs</th>
                        <td>{weaponStkPrime(limbs_dmg_range_1, burst)} B</td>
                        <td>{weaponStkPrime(limbs_dmg_range_2, burst)} B</td>
                        {range_3_used && <td>{weaponStkPrime(limbs_dmg_range_3, burst)} B</td>}
                        {range_4_used && <td>{weaponStkPrime(limbs_dmg_range_4, burst)} B</td>}
                    </tr>
                </tbody>
       



        );
   
    }
 
export default LocalisationStk3Range ;