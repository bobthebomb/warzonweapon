import React  from 'react';
import  { weaponDpsPrime } from "../WeaponMath"



const LocalisationDps3Range = (props) =>  {
    
        const { range_1, rpm, 
            range_3_used, range_4_used,
            head_dmg_range_1, head_dmg_range_2, head_dmg_range_3, head_dmg_range_4,
            chest_dmg_range_1, chest_dmg_range_2, chest_dmg_range_3, chest_dmg_range_4,
            stomach_dmg_range_1, stomach_dmg_range_2, stomach_dmg_range_3, stomach_dmg_range_4,
            limbs_dmg_range_1, limbs_dmg_range_2, limbs_dmg_range_3, limbs_dmg_range_4  } = props.weaponsDamage.weaponsData.weaponUpdated

   

         
        return (

            
                
                <tbody>
                    <tr>
                        <th>Head</th>
                        <td>{weaponDpsPrime(head_dmg_range_1, rpm)} dps</td>
                        <td>{weaponDpsPrime(head_dmg_range_2, rpm)} dps</td>
                        {range_3_used && <td>{weaponDpsPrime(head_dmg_range_3, rpm)} dps</td>}
                        {range_4_used && <td>{weaponDpsPrime(head_dmg_range_4, rpm)} dps</td>}
                    </tr>

                    <tr>
                        <th>Chest</th>
                        <td>{weaponDpsPrime(chest_dmg_range_1, rpm)} dps</td>
                        <td>{weaponDpsPrime(chest_dmg_range_2, rpm)} dps</td>
                        {range_3_used && <td>{weaponDpsPrime(chest_dmg_range_3, rpm)} dps</td>}
                        {range_4_used && <td>{weaponDpsPrime(chest_dmg_range_4, rpm)} dps</td>}
                    </tr>

                    <tr>
                        <th>Stomach</th>
                        <td>{weaponDpsPrime(stomach_dmg_range_1, rpm)} dps</td>
                        <td>{weaponDpsPrime(stomach_dmg_range_2, rpm)} dps</td>
                        {range_3_used && <td>{weaponDpsPrime(stomach_dmg_range_3, rpm)} dps</td>}
                        {range_4_used && <td>{weaponDpsPrime(stomach_dmg_range_4, rpm)} dps</td>}
                    </tr>
                    <tr>
                        <th>Limbs</th>
                        <td>{weaponDpsPrime(limbs_dmg_range_1, rpm)} dps</td>
                        <td>{weaponDpsPrime(limbs_dmg_range_2, rpm)} dps</td>
                        {range_3_used && <td>{weaponDpsPrime(limbs_dmg_range_3, rpm)} dps</td>}
                        {range_4_used && <td>{weaponDpsPrime(limbs_dmg_range_4, rpm)} dps</td>}
                    </tr>
                </tbody>
       



        );
   
    }
 
export default LocalisationDps3Range ;