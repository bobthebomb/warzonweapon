import React from 'react';
import { attachmentMath, renderTheStat } from "../WeaponMath"


const TableHead = (props) => {


    const {range_3_used, range_4_used, } = props.tableHead.weaponsData.weaponDetail


    const weaponUpdated = props.tableHead.weaponsData.weaponUpdated
    const weaponBase = props.tableHead.weaponsData.weaponDetail


return ( 
    
    
            <thead>
                <tr>
                <th>#</th>
                    {renderTheStat("range_1" , weaponBase, weaponUpdated, "M")}
                    {
                    weaponBase.range_3_used ? 
                    renderTheStat("range_2" , weaponBase, weaponUpdated, "M")
                    : <td> <i className="fas fa-infinity"></i></td>
                    }
                    { range_3_used && (weaponBase.range_4_used ? 
                    renderTheStat("range_3" , weaponBase, weaponUpdated, "M")
                    : <td><i className="fas fa-infinity"></i></td>)
                    }
                    {range_4_used && <th> <i className="fas fa-infinity"></i> </th>}
                </tr>
            </thead>

 );
}
 
export default TableHead;