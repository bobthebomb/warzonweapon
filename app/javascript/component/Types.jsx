import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Types extends Component {

    handleClick = () => {
        this.props.weaponsList(this.props.type.id)
    }

    render() {
        const active = this.props.active === this.props.type.id ? 
        "btn btn-grad active-button" 
        : 
        "btn btn-grad "

        return (
            <div className="col-4 col-lg-2 cardColumn ">
                <div className={active}  onClick={this.handleClick}
                     >
                        
                                    <p className="button-text">{this.props.type.name} </p>  

                </div>
            </div>
        );
    }
}


export default Types;