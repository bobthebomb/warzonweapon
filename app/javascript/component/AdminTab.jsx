import React, { Component } from 'react';

class AdminTab extends Component {
    state = { 
        title: ""
     }

     handleClick = () => {
        this.props.addToMeta()
     }

    render() { 
        return ( 
            <div>
                <button 
                style={{marginBottom: 20, marginTop: -5 }} 
                className="btn btn-outline-info btn-block"
                onClick={this.handleClick}
                > Add this weapon to meta </button>
            </div>
         );
    }
}
 
export default AdminTab;