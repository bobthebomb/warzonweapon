import React, { Component } from 'react';
import placeholder from '../images/placeholder.jpg'
import "./Weapons.scss"
import Axios from 'axios';
import ModalCanon from './weaponDetail/ModalComp/ModalCanon'
import ModalAmmo from './weaponDetail/ModalComp/ModalAmmo';
import ModalMuzzle from './weaponDetail/ModalComp/ModalMuzzle'
import ModalLaser from './weaponDetail/ModalComp/ModalLaser';
import ModalOptic from './weaponDetail/ModalComp/ModalOptic';
import ModalStock from './weaponDetail/ModalComp/ModalStock';
import ModalUnder from './weaponDetail/ModalComp/ModalUnder';
import ModalGrip from './weaponDetail/ModalComp/ModalGrip';
import ModalPerk from './weaponDetail/ModalComp/ModalPerks';

class WeaponImage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            canons: [],
            muzzles: [],
            lasers: [],
            optics: [],
            stocks: [],
            grips: [],
            perks: [],
            underbarrels: [],
            munitions: [],
            theSwitchWord: '',
           
        }
    }

  


    fetchAttachement = (attachement, switchWord) => {
        const weaponId = this.props.weaponState.weaponDetail.id
        Axios
            .get(`/api/v1/weapons/${weaponId}/${attachement}.json`)
           
            .then(response => {
                const attachements = response.data[attachement]
                this.setState({
                    [attachement]: [...attachements]
                }, () => {
                    this.setState({
                        theSwitchWord: switchWord
                    })
                })
            })
            .catch(error => console.log(error))
    }


    closeModal = (theWindow) => {
        this.setState({
            theSwitchWord: ''
        })
        
    }

    modalSwitch = (props) => {
        switch (props) {
            case "canonModalOpen":
                return <ModalCanon data={this.state.canons}
                    weaponNote={this.props.weaponState.weaponDetail.note}
                    closeModal={this.closeModal}            
                    handleArray={this.props.handleArray}
                />

            case "muzzleModalOpen":
                return <ModalMuzzle data={this.state.muzzles}
                    weaponNote={this.props.weaponState.weaponDetail.note}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                    
                   
                />
            case "lazerModalOpen":
                return <ModalLaser data={this.state.lasers}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                  
                />

            case "opticModalOpen":
                return <ModalOptic data={this.state.optics}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                
                />

            case "stockModalOpen":
                return <ModalStock data={this.state.stocks}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                 
                />

            case "underModalOpen":
                return <ModalUnder data={this.state.underbarrels}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                 
                />

            case "ammoModalOpen":
                return <ModalAmmo data={this.state.munitions}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                 
                />

            case "gripModalOpen":
                return <ModalGrip data={this.state.grips}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
            
                />

            case "perksModalOpen":
                return <ModalPerk data={this.state.perks}
                    handleArray={this.props.handleArray}
                    closeModal={this.closeModal}
                    
                />


            default:
                return null
        }
    }


    render() {

        const leSwitch = this.modalSwitch(this.state.theSwitchWord)
        const src = this.props.weaponState.weaponDetail.image ? this.props.weaponState.weaponDetail.image : placeholder
        const muzzle = this.props.weaponState.attachmentArray[0] ? this.props.weaponState.attachmentArray[0].name : "Muzzle"
        const barrel = this.props.weaponState.attachmentArray[1] ? this.props.weaponState.attachmentArray[1].name : "Barrel"
        const laser = this.props.weaponState.attachmentArray[2] ? this.props.weaponState.attachmentArray[2].name : "Laser"
        const optic = this.props.weaponState.attachmentArray[3] ? this.props.weaponState.attachmentArray[3].name : "Optic"
        const stock = this.props.weaponState.attachmentArray[4] ? this.props.weaponState.attachmentArray[4].name : "Stock"
        const under = this.props.weaponState.attachmentArray[5] ? this.props.weaponState.attachmentArray[5].name : "Under"
        const munition = this.props.weaponState.attachmentArray[6] ? this.props.weaponState.attachmentArray[6].name : "Ammo"
        const grip = this.props.weaponState.attachmentArray[7] ? this.props.weaponState.attachmentArray[7].name : "Grip"
        const perk = this.props.weaponState.attachmentArray[8] ? this.props.weaponState.attachmentArray[8].name : "Perk"
        
        return (

            



            <div className="row">


                <div className="col-xs-12 col-lg-8 offset-lg-2">


                    <div className="row ">
                        <div className="col" >
                            <button onClick={() => this.fetchAttachement("muzzles", "muzzleModalOpen")} className="menu-top btn btn-outline-info btn-block btn-lg" id="hello">{muzzle}</button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("canons", "canonModalOpen")} className="menu-top btn btn-outline-info btn-block btn-lg" id="hello"> {barrel}</button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("lasers", "lazerModalOpen")} className="menu-top btn btn-outline-info btn-block btn-lg" id="hello">{laser}</button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("optics", "opticModalOpen")} className="menu-top btn btn-outline-info btn-block btn-lg" id="hello">{optic}</button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("stocks", "stockModalOpen")} className="menu-top btn btn-outline-info btn-block btn-lg" id="hello">{stock}</button>
                        </div>

                    </div>
                    <div className="row ">
                        <div className="col d-flex justify-content-center">
                            <img src={src} className="img-fluid float-center" alt="snow" />
                          
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <button onClick={() => this.fetchAttachement("underbarrels", "underModalOpen")} className="menu-bottom btn btn-outline-info btn-block btn-lg" id="hello">{under}</button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("munitions", "ammoModalOpen")} className="menu-bottom btn btn-outline-info btn-block btn-lg" id="hello">{munition}</button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("grips", "gripModalOpen")} className="menu-bottom btn btn-outline-info btn-block btn-lg" id="hello"> {grip} </button>
                        </div>

                        <div className="col">
                            <button onClick={() => this.fetchAttachement("perks", "perksModalOpen")} className="menu-bottom btn btn-outline-info btn-block btn-lg" id="hello">{perk}</button>
                        </div>



                    </div>



                </div>
                {leSwitch}
            </div>
        );
    }
}

export default WeaponImage;