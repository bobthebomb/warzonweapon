import React, { Component } from 'react';
import './Weapons.scss'

class Weapons extends Component {

    handleClick = () => {
        this.props.weaponDetail(this.props.weapon.id)
    }

    render() {
        const active = this.props.activeWeapon === this.props.weapon.id ?  "btn btn-grad active-button" 
        : 
        "btn btn-grad "
        return (
            <div className="col-4 col-lg-2 cardColumn">
                <div className={active} onClick={this.handleClick}>
                    
                      <p className="button-text"> {this.props.weapon.name} </p>  
                    
                </div>
            </div>
        );
    }
}

export default Weapons;