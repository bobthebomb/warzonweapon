require 'test_helper'

class Api::V1::StockRecoilsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_v1_stock_recoils_index_url
    assert_response :success
  end

  test "should get edit" do
    get api_v1_stock_recoils_edit_url
    assert_response :success
  end

  test "should get new" do
    get api_v1_stock_recoils_new_url
    assert_response :success
  end

end
