require 'test_helper'

class Api::V1::CanonRecoilsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get api_v1_canon_recoils_new_url
    assert_response :success
  end

  test "should get index" do
    get api_v1_canon_recoils_index_url
    assert_response :success
  end

  test "should get edit" do
    get api_v1_canon_recoils_edit_url
    assert_response :success
  end

end
