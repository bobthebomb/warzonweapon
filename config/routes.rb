Rails.application.routes.draw do
 
  
  
  
  namespace :api do
    namespace :v1 do
      
      resources :setups
      resources :types 
      resources :weapons do
        resources :hip_fires, module: :weapons
        resources :canons do
          resources :canon_recoils
          resources :hip_fires, module: :canons
        end
        resources :muzzles do
            resources :muzzle_recoils
            resources :hip_fires, module: :muzzles
        end
        resources :lasers do
          resources :laser_recoils
          resources :hip_fires, module: :lasers
        end
        resources :optics do 
          resources :optic_recoils
          resources :hip_fires, module: :optics
        end 
        resources :stocks do
          resources :stock_recoils
          resources :hip_fires, module: :stocks
        end
        resources :underbarrels do
          resources :underbarrel_recoils
          resources :hip_fires, module: :underbarrels
        end
        resources :munitions do
          resources :munition_recoils
          resources :hip_fires, module: :munitions
        end
        resources :grips do
          resources :grip_recoils
          resources :hip_fires, module: :grips
        end
        resources :perks
        resources :recoils
      end
    end
  end
  
resources :blogs
devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
 root to: "blogs#index"
 get '/admin', to: "pages#admin"
 get '/warzone-weapons-builder', to: "pages#reactor"
 get 'api/v1/user', to: "pages#user"
 get '/table', to: "pages#table"

#  get '*path', to: "pages#reactor" 
end
  