class ChangeReloadTimeToDecimal < ActiveRecord::Migration[6.0]
  def change
    change_column :weapons, :reload_time, :decimal
  end
end
