class AddMetaIdToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :meta_id, :string, default: ""
    change_column :setups, :reload_time, :float, default: 0
    change_column :canons, :reload_time, :float, default: 0
    change_column :muzzles, :reload_time, :float, default: 0
    change_column :lasers, :reload_time, :float, default: 0
    change_column :optics, :reload_time, :float, default: 0
    change_column :stocks, :reload_time, :float, default: 0
    change_column :underbarrels, :reload_time, :float, default: 0
    change_column :munitions, :reload_time, :float, default: 0
    change_column :grips, :reload_time, :float, default: 0
    change_column :perks, :reload_time, :float, default: 0

    

  end
end
