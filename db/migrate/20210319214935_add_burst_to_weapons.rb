class AddBurstToWeapons < ActiveRecord::Migration[6.0]
  def change
    add_column :weapons, :burst, :float, default: 0 
  end
end
