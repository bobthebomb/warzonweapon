class AddIlluShowToBlogs < ActiveRecord::Migration[6.0]
  def change

    add_column :blogs, :show_illustration, :boolean, default: true

  end
end
