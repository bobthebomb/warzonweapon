class AddRpmToGrips < ActiveRecord::Migration[6.0]
  def change
    add_column :grips, :movement, :float, default: 0
    add_column :grips, :rpm, :integer, default: 0
    add_column :grips, :ads_time, :integer, default: 0
    add_column :grips, :sprint_to_fire, :integer, default: 0
    add_column :grips, :hip_fire_cone, :integer, default: 0
    add_column :grips, :bullet_velocity, :integer, default: 0
    add_column :grips, :bounce, :integer, default: 0
    add_column :grips, :vertical_recoil, :integer, default: 0
    add_column :grips, :horizontal_recoil, :integer, default: 0
    add_column :grips, :vertical_recoil_first_10, :integer, default: 0
    add_column :grips, :horizontal_recoil_first_10, :integer, default: 0
    add_column :grips, :vertical_recoil_first_20, :integer, default: 0
    add_column :grips, :horizontal_recoil_first_20, :integer, default: 0
    add_column :grips, :head_dmg_range_1, :integer, default: 0
    add_column :grips, :chest_dmg_range_1, :integer, default: 0
    add_column :grips, :stomach_dmg_range_1, :integer, default: 0
    add_column :grips, :limbs_dmg_range_1, :integer, default: 0
    add_column :grips, :head_dmg_range_2, :integer, default: 0
    add_column :grips, :chest_dmg_range_2, :integer, default: 0
    add_column :grips, :stomach_dmg_range_2, :integer, default: 0
    add_column :grips, :limbs_dmg_range_2, :integer, default: 0
    add_column :grips, :head_dmg_range_3, :integer, default: 0
    add_column :grips, :chest_dmg_range_3, :integer, default: 0
    add_column :grips, :stomach_dmg_range_3, :integer, default: 0
    add_column :grips, :limbs_dmg_range_3, :integer, default: 0
    add_column :grips, :head_dmg_range_4, :integer, default: 0
    add_column :grips, :chest_dmg_range_4, :integer, default: 0
    add_column :grips, :stomach_dmg_range_4, :integer, default: 0
    add_column :grips, :limbs_dmg_range_4, :integer, default: 0
    add_column :grips, :spread_image, :string, default: ''
    add_column :grips, :game, :string, default: ''
    add_column :grips, :note, :string, default: ''
    add_column :grips, :range_1, :float, default: 0
    add_column :grips, :range_2, :float, default: 0
    add_column :grips, :range_3, :float, default: 0
    add_column :grips, :range_3_used, :boolean, default: false
    add_column :grips, :range_4, :float, default: 0
    add_column :grips, :range_4_used, :boolean, default: false
    add_column :grips, :reload_time, :decimal
  end
end
