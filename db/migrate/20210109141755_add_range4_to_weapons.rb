class AddRange4ToWeapons < ActiveRecord::Migration[6.0]
  def change
    add_column :weapons, :range_4, :float, default: 0
    add_column :weapons, :range_4_used, :boolean, default: false
  end
end
