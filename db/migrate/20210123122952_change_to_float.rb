class ChangeToFloat < ActiveRecord::Migration[6.0]
  def change
    change_column :muzzles, :rpm, :float, default: 0
    change_column :canons, :rpm, :float, default: 0
    change_column :lasers, :rpm, :float, default: 0
    change_column :optics, :rpm, :float, default: 0
    change_column :stocks, :rpm, :float, default: 0
    change_column :underbarrels, :rpm, :float, default: 0
    change_column :munitions, :rpm, :float, default: 0
    change_column :grips, :rpm, :float, default: 0

    change_column :muzzles, :ads_time, :float, default: 0
    change_column :canons, :ads_time, :float, default: 0
    change_column :lasers, :ads_time, :float, default: 0
    change_column :optics, :ads_time, :float, default: 0
    change_column :stocks, :ads_time, :float, default: 0
    change_column :underbarrels, :ads_time, :float, default: 0
    change_column :munitions, :ads_time, :float, default: 0
    change_column :grips, :ads_time, :float, default: 0

    change_column :muzzles, :sprint_to_fire, :float, default: 0
    change_column :canons, :sprint_to_fire, :float, default: 0
    change_column :lasers, :sprint_to_fire, :float, default: 0
    change_column :optics, :sprint_to_fire, :float, default: 0
    change_column :stocks, :sprint_to_fire, :float, default: 0
    change_column :underbarrels, :sprint_to_fire, :float, default: 0
    change_column :munitions, :sprint_to_fire, :float, default: 0
    change_column :grips, :sprint_to_fire, :float, default: 0

    change_column :muzzles, :hip_fire_cone, :float, default: 0
    change_column :canons, :hip_fire_cone, :float, default: 0
    change_column :lasers, :hip_fire_cone, :float, default: 0
    change_column :optics, :hip_fire_cone, :float, default: 0
    change_column :stocks, :hip_fire_cone, :float, default: 0
    change_column :underbarrels, :hip_fire_cone, :float, default: 0
    change_column :munitions, :hip_fire_cone, :float, default: 0
    change_column :grips, :hip_fire_cone, :float, default: 0

    change_column :muzzles, :bullet_velocity, :float, default: 0
    change_column :canons, :bullet_velocity, :float, default: 0
    change_column :lasers, :bullet_velocity, :float, default: 0
    change_column :optics, :bullet_velocity, :float, default: 0
    change_column :stocks, :bullet_velocity, :float, default: 0
    change_column :underbarrels, :bullet_velocity, :float, default: 0
    change_column :munitions, :bullet_velocity, :float, default: 0
    change_column :grips, :bullet_velocity, :float, default: 0

    change_column :muzzles, :bounce, :float, default: 0
    change_column :canons, :bounce, :float, default: 0
    change_column :lasers, :bounce, :float, default: 0
    change_column :optics, :bounce, :float, default: 0
    change_column :stocks, :bounce, :float, default: 0
    change_column :underbarrels, :bounce, :float, default: 0
    change_column :munitions, :bounce, :float, default: 0
    change_column :grips, :bounce, :float, default: 0

    change_column :muzzles, :vertical_recoil, :float, default: 0
    change_column :canons, :vertical_recoil, :float, default: 0
    change_column :lasers, :vertical_recoil, :float, default: 0
    change_column :optics, :vertical_recoil, :float, default: 0
    change_column :stocks, :vertical_recoil, :float, default: 0
    change_column :underbarrels, :vertical_recoil, :float, default: 0
    change_column :munitions, :vertical_recoil, :float, default: 0
    change_column :grips, :vertical_recoil, :float, default: 0

    change_column :muzzles, :horizontal_recoil, :float, default: 0
    change_column :canons, :horizontal_recoil, :float, default: 0
    change_column :lasers, :horizontal_recoil, :float, default: 0
    change_column :optics, :horizontal_recoil, :float, default: 0
    change_column :stocks, :horizontal_recoil, :float, default: 0
    change_column :underbarrels, :horizontal_recoil, :float, default: 0
    change_column :munitions, :horizontal_recoil, :float, default: 0
    change_column :grips, :horizontal_recoil, :float, default: 0

    change_column :muzzles, :horizontal_recoil_first_10, :float, default: 0
    change_column :canons, :horizontal_recoil_first_10, :float, default: 0
    change_column :lasers, :horizontal_recoil_first_10, :float, default: 0
    change_column :optics, :horizontal_recoil_first_10, :float, default: 0
    change_column :stocks, :horizontal_recoil_first_10, :float, default: 0
    change_column :underbarrels, :horizontal_recoil_first_10, :float, default: 0
    change_column :munitions, :horizontal_recoil_first_10, :float, default: 0
    change_column :grips, :horizontal_recoil_first_10, :float, default: 0

    change_column :muzzles, :horizontal_recoil_first_20, :float, default: 0
    change_column :canons, :horizontal_recoil_first_20, :float, default: 0
    change_column :lasers, :horizontal_recoil_first_20, :float, default: 0
    change_column :optics, :horizontal_recoil_first_20, :float, default: 0
    change_column :stocks, :horizontal_recoil_first_20, :float, default: 0
    change_column :underbarrels, :horizontal_recoil_first_20, :float, default: 0
    change_column :munitions, :horizontal_recoil_first_20, :float, default: 0
    change_column :grips, :horizontal_recoil_first_20, :float, default: 0

    change_column :muzzles, :vertical_recoil_first_10, :float, default: 0
    change_column :canons, :vertical_recoil_first_10, :float, default: 0
    change_column :lasers, :vertical_recoil_first_10, :float, default: 0
    change_column :optics, :vertical_recoil_first_10, :float, default: 0
    change_column :stocks, :vertical_recoil_first_10, :float, default: 0
    change_column :underbarrels, :vertical_recoil_first_10, :float, default: 0
    change_column :munitions, :vertical_recoil_first_10, :float, default: 0
    change_column :grips, :vertical_recoil_first_10, :float, default: 0

    change_column :muzzles, :vertical_recoil_first_20, :float, default: 0
    change_column :canons, :vertical_recoil_first_20, :float, default: 0
    change_column :lasers, :vertical_recoil_first_20, :float, default: 0
    change_column :optics, :vertical_recoil_first_20, :float, default: 0
    change_column :stocks, :vertical_recoil_first_20, :float, default: 0
    change_column :underbarrels, :vertical_recoil_first_20, :float, default: 0
    change_column :munitions, :vertical_recoil_first_20, :float, default: 0
    change_column :grips, :vertical_recoil_first_20, :float, default: 0

    change_column :weapons, :ads_time, :float, default: 0
    change_column :weapons, :sprint_to_fire, :float, default: 0

  end
end
