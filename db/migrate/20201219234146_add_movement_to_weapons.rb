class AddMovementToWeapons < ActiveRecord::Migration[6.0]
  def change
    
    add_column :weapons, :magazine, :integer, default: 0
    add_column :weapons, :reload_time, :integer, default: 0
    add_column :weapons, :ads_time, :integer, default: 0
    add_column :weapons, :sprint_to_fire, :integer, default: 0
    add_column :weapons, :movement, :float, default: 0
    add_column :weapons, :ads_movement, :float, default: 0
    add_column :weapons, :hip_fire_cone, :decimal, default: 0
    add_column :weapons, :bullet_velocity, :decimal, default: 0
    add_column :weapons, :bounce, :decimal, default: 0
    add_column :weapons, :vertical_recoil, :decimal, default: 0
    add_column :weapons, :horizontal_recoil, :decimal, default: 0
    add_column :weapons, :vertical_recoil_first_10, :decimal, default: 0
    add_column :weapons, :horizontal_recoil_first_10, :decimal, default: 0
    add_column :weapons, :vertical_recoil_first_20, :decimal, default: 0
    add_column :weapons, :horizontal_recoil_first_20, :decimal, default: 0
    add_column :weapons, :head_dmg_range_1, :integer, default: 0
    add_column :weapons, :chest_dmg_range_1, :integer, default: 0
    add_column :weapons, :stomach_dmg_range_1, :integer, default: 0
    add_column :weapons, :limbs_dmg_range_1, :integer, default: 0
    add_column :weapons, :head_dmg_range_2, :integer, default: 0
    add_column :weapons, :chest_dmg_range_2, :integer, default: 0
    add_column :weapons, :stomach_dmg_range_2, :integer, default: 0
    add_column :weapons, :limbs_dmg_range_2, :integer, default: 0
    add_column :weapons, :head_dmg_range_3, :integer, default: 0
    add_column :weapons, :chest_dmg_range_3, :integer, default: 0
    add_column :weapons, :stomach_dmg_range_3, :integer, default: 0
    add_column :weapons, :limbs_dmg_range_3, :integer, default: 0
    add_column :weapons, :head_dmg_range_4, :integer, default: 0
    add_column :weapons, :chest_dmg_range_4, :integer, default: 0
    add_column :weapons, :stomach_dmg_range_4, :integer, default: 0
    add_column :weapons, :limbs_dmg_range_4, :integer, default: 0
    add_column :weapons, :spread_image, :string, default: ''

  end
end
