class CreateMunitions < ActiveRecord::Migration[6.0]
  def change
    create_table :munitions do |t|
      t.string :name
      t.integer :magazine
      t.references :weapon, null: false, foreign_key: true

      t.timestamps
    end
  end
end
