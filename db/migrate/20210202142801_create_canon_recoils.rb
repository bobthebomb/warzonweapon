class CreateCanonRecoils < ActiveRecord::Migration[6.0]
  def change
    create_table :canon_recoils do |t|
      t.string :name
      
      t.references :canon, null: false, foreign_key: true
      t.timestamps
    end
  end
end
