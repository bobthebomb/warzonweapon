class CreateOpticRecoils < ActiveRecord::Migration[6.0]
  def change
    create_table :optic_recoils do |t|
      t.string :name
      
      t.references :optic, null: false, foreign_key: true
      t.timestamps
    end
  end
end
