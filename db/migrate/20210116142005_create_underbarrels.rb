class CreateUnderbarrels < ActiveRecord::Migration[6.0]
  def change
    create_table :underbarrels do |t|
      t.string :name
      t.references :weapon, null: false, foreign_key: true

      t.timestamps
    end
  end
end
