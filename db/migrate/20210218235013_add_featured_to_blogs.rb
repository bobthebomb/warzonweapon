class AddFeaturedToBlogs < ActiveRecord::Migration[6.0]
  def change
    add_column :blogs, :featured, :integer, default: 0
  end
end
