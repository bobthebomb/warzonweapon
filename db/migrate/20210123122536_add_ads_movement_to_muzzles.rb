class AddAdsMovementToMuzzles < ActiveRecord::Migration[6.0]
  def change
    add_column :muzzles, :ads_movement, :float, default: 0
    add_column :canons, :ads_movement, :float, default: 0
    add_column :lasers, :ads_movement, :float, default: 0
    add_column :optics, :ads_movement, :float, default: 0
    add_column :stocks, :ads_movement, :float, default: 0
    add_column :underbarrels, :ads_movement, :float, default: 0
    add_column :munitions, :ads_movement, :float, default: 0
    add_column :grips, :ads_movement, :float, default: 0


  end
end
