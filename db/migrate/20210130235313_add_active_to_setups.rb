class AddActiveToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :active, :boolean, default: false
  end
end
