class CreateUnderbarrelRecoils < ActiveRecord::Migration[6.0]
  def change
    create_table :underbarrel_recoils do |t|
      t.string :name
      
      t.references :underbarrel, null: false, foreign_key: true
      t.timestamps
    end
  end
end
