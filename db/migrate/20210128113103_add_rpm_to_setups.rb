class AddRpmToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :movement, :float, default: 0
    add_column :setups, :rpm, :float, default: 0
    add_column :setups, :ads_time, :float, default: 0
    add_column :setups, :sprint_to_fire, :float, default: 0
    add_column :setups, :hip_fire_cone, :float, default: 0
    add_column :setups, :bullet_velocity, :float, default: 0
    add_column :setups, :bounce, :float, default: 0
    add_column :setups, :vertical_recoil, :float, default: 0
    add_column :setups, :horizontal_recoil, :float, default: 0
    add_column :setups, :vertical_recoil_first_10, :float, default: 0
    add_column :setups, :horizontal_recoil_first_10, :float, default: 0
    add_column :setups, :vertical_recoil_first_20, :float, default: 0
    add_column :setups, :horizontal_recoil_first_20, :float, default: 0
    add_column :setups, :head_dmg_range_1, :integer, default: 0
    add_column :setups, :chest_dmg_range_1, :integer, default: 0
    add_column :setups, :stomach_dmg_range_1, :integer, default: 0
    add_column :setups, :limbs_dmg_range_1, :integer, default: 0
    add_column :setups, :head_dmg_range_2, :integer, default: 0
    add_column :setups, :chest_dmg_range_2, :integer, default: 0
    add_column :setups, :stomach_dmg_range_2, :integer, default: 0
    add_column :setups, :limbs_dmg_range_2, :integer, default: 0
    add_column :setups, :head_dmg_range_3, :integer, default: 0
    add_column :setups, :chest_dmg_range_3, :integer, default: 0
    add_column :setups, :stomach_dmg_range_3, :integer, default: 0
    add_column :setups, :limbs_dmg_range_3, :integer, default: 0
    add_column :setups, :head_dmg_range_4, :integer, default: 0
    add_column :setups, :chest_dmg_range_4, :integer, default: 0
    add_column :setups, :stomach_dmg_range_4, :integer, default: 0
    add_column :setups, :limbs_dmg_range_4, :integer, default: 0
    add_column :setups, :spread_image, :string, default: ''
    add_column :setups, :game, :string, default: ''
    add_column :setups, :note, :string, default: ''
    add_column :setups, :range_1, :float, default: 0
    add_column :setups, :range_2, :float, default: 0
    add_column :setups, :range_3, :float, default: 0
    add_column :setups, :range_3_used, :boolean, default: false
    add_column :setups, :range_4, :float, default: 0
    add_column :setups, :range_4_used, :boolean, default: false
    add_column :setups, :reload_time, :float
    add_column :setups, :ads_movement, :float, default: 0
    add_column :setups, :firing_movement, :float, default: 0

  end
end
