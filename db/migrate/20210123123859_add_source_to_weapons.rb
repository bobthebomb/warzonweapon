class AddSourceToWeapons < ActiveRecord::Migration[6.0]
  def change

    add_column :weapons, :muzzles_notes, :text, default: ""
    add_column :weapons, :canons_notes, :text, default: ""
    add_column :weapons, :lasers_notes, :text, default: ""
    add_column :weapons, :optics_notes, :text, default: ""
    add_column :weapons, :stocks_notes, :text, default: ""
    add_column :weapons, :underbarrels_notes, :text, default: ""
    add_column :weapons, :munitions_notes, :text, default: ""
    add_column :weapons, :grips_notes, :text, default: ""
    add_column :weapons, :perks_notes, :text, default: ""
    
  end
end
