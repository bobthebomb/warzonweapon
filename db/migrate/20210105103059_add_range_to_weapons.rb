class AddRangeToWeapons < ActiveRecord::Migration[6.0]
  def change
    add_column :weapons, :range_1, :float, default: 0
    add_column :weapons, :range_2, :float, default: 0
    add_column :weapons, :range_3, :float, default: 0
    add_column :weapons, :range_3_used, :boolean, default: false
  end
end
