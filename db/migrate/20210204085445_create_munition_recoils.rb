class CreateMunitionRecoils < ActiveRecord::Migration[6.0]
  def change
    create_table :munition_recoils do |t|
      t.string :name
      
      t.references :munition, null: false, foreign_key: true
      t.timestamps
    end
  end
end
