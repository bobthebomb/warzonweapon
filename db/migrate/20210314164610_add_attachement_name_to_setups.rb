class AddAttachementNameToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :attachement_names, :string, array: true
  end
end
