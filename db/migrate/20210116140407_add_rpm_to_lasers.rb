class AddRpmToLasers < ActiveRecord::Migration[6.0]
  def change
    add_column :lasers, :movement, :float, default: 0
    add_column :lasers, :rpm, :integer, default: 0
    add_column :lasers, :ads_time, :integer, default: 0
    add_column :lasers, :sprint_to_fire, :integer, default: 0
    add_column :lasers, :hip_fire_cone, :integer, default: 0
    add_column :lasers, :bullet_velocity, :integer, default: 0
    add_column :lasers, :bounce, :integer, default: 0
    add_column :lasers, :vertical_recoil, :integer, default: 0
    add_column :lasers, :horizontal_recoil, :integer, default: 0
    add_column :lasers, :vertical_recoil_first_10, :integer, default: 0
    add_column :lasers, :horizontal_recoil_first_10, :integer, default: 0
    add_column :lasers, :vertical_recoil_first_20, :integer, default: 0
    add_column :lasers, :horizontal_recoil_first_20, :integer, default: 0
    add_column :lasers, :head_dmg_range_1, :integer, default: 0
    add_column :lasers, :chest_dmg_range_1, :integer, default: 0
    add_column :lasers, :stomach_dmg_range_1, :integer, default: 0
    add_column :lasers, :limbs_dmg_range_1, :integer, default: 0
    add_column :lasers, :head_dmg_range_2, :integer, default: 0
    add_column :lasers, :chest_dmg_range_2, :integer, default: 0
    add_column :lasers, :stomach_dmg_range_2, :integer, default: 0
    add_column :lasers, :limbs_dmg_range_2, :integer, default: 0
    add_column :lasers, :head_dmg_range_3, :integer, default: 0
    add_column :lasers, :chest_dmg_range_3, :integer, default: 0
    add_column :lasers, :stomach_dmg_range_3, :integer, default: 0
    add_column :lasers, :limbs_dmg_range_3, :integer, default: 0
    add_column :lasers, :head_dmg_range_4, :integer, default: 0
    add_column :lasers, :chest_dmg_range_4, :integer, default: 0
    add_column :lasers, :stomach_dmg_range_4, :integer, default: 0
    add_column :lasers, :limbs_dmg_range_4, :integer, default: 0
    add_column :lasers, :spread_image, :string, default: ''
    add_column :lasers, :game, :string, default: ''
    add_column :lasers, :note, :string, default: ''
    add_column :lasers, :range_1, :float, default: 0
    add_column :lasers, :range_2, :float, default: 0
    add_column :lasers, :range_3, :float, default: 0
    add_column :lasers, :range_3_used, :boolean, default: false
    add_column :lasers, :range_4, :float, default: 0
    add_column :lasers, :range_4_used, :boolean, default: false
    add_column :lasers, :reload_time, :decimal
  end
end
