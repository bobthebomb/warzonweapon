class AddMagazineToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :magazine, :integer
  end
end
