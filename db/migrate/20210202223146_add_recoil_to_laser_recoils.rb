class AddRecoilToLaserRecoils < ActiveRecord::Migration[6.0]
  def change

    add_column :laser_recoils, :hip_fire_cone, :float, default: 0
    add_column :laser_recoils, :bounce, :float, default: 0
    add_column :laser_recoils, :vertical_recoil, :float, default: 0
    add_column :laser_recoils, :horizontal_recoil, :float, default: 0
    add_column :laser_recoils, :vertical_recoil_first_10, :float, default: 0
    add_column :laser_recoils, :horizontal_recoil_first_10, :float, default: 0
    add_column :laser_recoils, :vertical_recoil_first_20, :float, default: 0
    add_column :laser_recoils, :horizontal_recoil_first_20, :float, default: 0
    add_column :laser_recoils, :vertical_recoil_first_0, :float, default: 0
    add_column :laser_recoils, :horizontal_recoil_first_0, :float, default: 0
  end
end
