class CreatePerks < ActiveRecord::Migration[6.0]
  def change
    create_table :perks do |t|
      t.string :name
      t.text  :effect
      t.references :weapon, null: false, foreign_key: true

      t.timestamps
    end
  end
end
