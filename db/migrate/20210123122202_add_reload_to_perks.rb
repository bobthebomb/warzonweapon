class AddReloadToPerks < ActiveRecord::Migration[6.0]
  def change
    add_column :perks, :reload_time, :float, default: 0
    change_column :weapons, :rpm, :float

  end
end
