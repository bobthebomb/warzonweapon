class AddMagazineDefaultToSetup < ActiveRecord::Migration[6.0]
  def change
    change_column :setups, :magazine, :integer, default: 0
  end
end
