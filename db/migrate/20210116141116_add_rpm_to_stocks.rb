class AddRpmToStocks < ActiveRecord::Migration[6.0]
  def change
    add_column :stocks, :movement, :float, default: 0
    add_column :stocks, :rpm, :integer, default: 0
    add_column :stocks, :ads_time, :integer, default: 0
    add_column :stocks, :sprint_to_fire, :integer, default: 0
    add_column :stocks, :hip_fire_cone, :integer, default: 0
    add_column :stocks, :bullet_velocity, :integer, default: 0
    add_column :stocks, :bounce, :integer, default: 0
    add_column :stocks, :vertical_recoil, :integer, default: 0
    add_column :stocks, :horizontal_recoil, :integer, default: 0
    add_column :stocks, :vertical_recoil_first_10, :integer, default: 0
    add_column :stocks, :horizontal_recoil_first_10, :integer, default: 0
    add_column :stocks, :vertical_recoil_first_20, :integer, default: 0
    add_column :stocks, :horizontal_recoil_first_20, :integer, default: 0
    add_column :stocks, :head_dmg_range_1, :integer, default: 0
    add_column :stocks, :chest_dmg_range_1, :integer, default: 0
    add_column :stocks, :stomach_dmg_range_1, :integer, default: 0
    add_column :stocks, :limbs_dmg_range_1, :integer, default: 0
    add_column :stocks, :head_dmg_range_2, :integer, default: 0
    add_column :stocks, :chest_dmg_range_2, :integer, default: 0
    add_column :stocks, :stomach_dmg_range_2, :integer, default: 0
    add_column :stocks, :limbs_dmg_range_2, :integer, default: 0
    add_column :stocks, :head_dmg_range_3, :integer, default: 0
    add_column :stocks, :chest_dmg_range_3, :integer, default: 0
    add_column :stocks, :stomach_dmg_range_3, :integer, default: 0
    add_column :stocks, :limbs_dmg_range_3, :integer, default: 0
    add_column :stocks, :head_dmg_range_4, :integer, default: 0
    add_column :stocks, :chest_dmg_range_4, :integer, default: 0
    add_column :stocks, :stomach_dmg_range_4, :integer, default: 0
    add_column :stocks, :limbs_dmg_range_4, :integer, default: 0
    add_column :stocks, :spread_image, :string, default: ''
    add_column :stocks, :game, :string, default: ''
    add_column :stocks, :note, :string, default: ''
    add_column :stocks, :range_1, :float, default: 0
    add_column :stocks, :range_2, :float, default: 0
    add_column :stocks, :range_3, :float, default: 0
    add_column :stocks, :range_3_used, :boolean, default: false
    add_column :stocks, :range_4, :float, default: 0
    add_column :stocks, :range_4_used, :boolean, default: false
    add_column :stocks, :reload_time, :decimal
  end
end
