class AddRpmToMunitions < ActiveRecord::Migration[6.0]
  def change
    add_column :munitions, :movement, :float, default: 0
    add_column :munitions, :rpm, :integer, default: 0
    add_column :munitions, :ads_time, :integer, default: 0
    add_column :munitions, :sprint_to_fire, :integer, default: 0
    add_column :munitions, :hip_fire_cone, :integer, default: 0
    add_column :munitions, :bullet_velocity, :integer, default: 0
    add_column :munitions, :bounce, :integer, default: 0
    add_column :munitions, :vertical_recoil, :integer, default: 0
    add_column :munitions, :horizontal_recoil, :integer, default: 0
    add_column :munitions, :vertical_recoil_first_10, :integer, default: 0
    add_column :munitions, :horizontal_recoil_first_10, :integer, default: 0
    add_column :munitions, :vertical_recoil_first_20, :integer, default: 0
    add_column :munitions, :horizontal_recoil_first_20, :integer, default: 0
    add_column :munitions, :head_dmg_range_1, :integer, default: 0
    add_column :munitions, :chest_dmg_range_1, :integer, default: 0
    add_column :munitions, :stomach_dmg_range_1, :integer, default: 0
    add_column :munitions, :limbs_dmg_range_1, :integer, default: 0
    add_column :munitions, :head_dmg_range_2, :integer, default: 0
    add_column :munitions, :chest_dmg_range_2, :integer, default: 0
    add_column :munitions, :stomach_dmg_range_2, :integer, default: 0
    add_column :munitions, :limbs_dmg_range_2, :integer, default: 0
    add_column :munitions, :head_dmg_range_3, :integer, default: 0
    add_column :munitions, :chest_dmg_range_3, :integer, default: 0
    add_column :munitions, :stomach_dmg_range_3, :integer, default: 0
    add_column :munitions, :limbs_dmg_range_3, :integer, default: 0
    add_column :munitions, :head_dmg_range_4, :integer, default: 0
    add_column :munitions, :chest_dmg_range_4, :integer, default: 0
    add_column :munitions, :stomach_dmg_range_4, :integer, default: 0
    add_column :munitions, :limbs_dmg_range_4, :integer, default: 0
    add_column :munitions, :spread_image, :string, default: ''
    add_column :munitions, :game, :string, default: ''
    add_column :munitions, :note, :string, default: ''
    add_column :munitions, :range_1, :float, default: 0
    add_column :munitions, :range_2, :float, default: 0
    add_column :munitions, :range_3, :float, default: 0
    add_column :munitions, :range_3_used, :boolean, default: false
    add_column :munitions, :range_4, :float, default: 0
    add_column :munitions, :range_4_used, :boolean, default: false
    add_column :munitions, :reload_time, :decimal
  end
end
