class AddMovementToCanons < ActiveRecord::Migration[6.0]
  def change
    add_column :canons, :movement, :float, default: 0
    add_column :canons, :rpm, :integer, default: 0
    add_column :canons, :ads_time, :integer, default: 0
    add_column :canons, :sprint_to_fire, :integer, default: 0
    add_column :canons, :hip_fire_cone, :integer, default: 0
    add_column :canons, :bullet_velocity, :integer, default: 0
    add_column :canons, :bounce, :integer, default: 0
    add_column :canons, :vertical_recoil, :integer, default: 0
    add_column :canons, :horizontal_recoil, :integer, default: 0
    add_column :canons, :vertical_recoil_first_10, :integer, default: 0
    add_column :canons, :horizontal_recoil_first_10, :integer, default: 0
    add_column :canons, :vertical_recoil_first_20, :integer, default: 0
    add_column :canons, :horizontal_recoil_first_20, :integer, default: 0
    add_column :canons, :head_dmg_range_1, :integer, default: 0
    add_column :canons, :chest_dmg_range_1, :integer, default: 0
    add_column :canons, :stomach_dmg_range_1, :integer, default: 0
    add_column :canons, :limbs_dmg_range_1, :integer, default: 0
    add_column :canons, :head_dmg_range_2, :integer, default: 0
    add_column :canons, :chest_dmg_range_2, :integer, default: 0
    add_column :canons, :stomach_dmg_range_2, :integer, default: 0
    add_column :canons, :limbs_dmg_range_2, :integer, default: 0
    add_column :canons, :head_dmg_range_3, :integer, default: 0
    add_column :canons, :chest_dmg_range_3, :integer, default: 0
    add_column :canons, :stomach_dmg_range_3, :integer, default: 0
    add_column :canons, :limbs_dmg_range_3, :integer, default: 0
    add_column :canons, :head_dmg_range_4, :integer, default: 0
    add_column :canons, :chest_dmg_range_4, :integer, default: 0
    add_column :canons, :stomach_dmg_range_4, :integer, default: 0
    add_column :canons, :limbs_dmg_range_4, :integer, default: 0
    add_column :canons, :spread_image, :string, default: ''
    add_column :canons, :game, :string, default: ''
    add_column :canons, :note, :string, default: ''
    add_column :canons, :range_1, :float, default: 0
    add_column :canons, :range_2, :float, default: 0
    add_column :canons, :range_3, :float, default: 0
    add_column :canons, :range_3_used, :boolean, default: false
    add_column :canons, :range_4, :float, default: 0
    add_column :canons, :range_4_used, :boolean, default: false
    add_column :canons, :reload_time, :decimal

  end
end
