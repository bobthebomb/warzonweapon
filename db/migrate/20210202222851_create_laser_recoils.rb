class CreateLaserRecoils < ActiveRecord::Migration[6.0]
  def change
    create_table :laser_recoils do |t|
      t.string :name
      
      t.references :laser, null: false, foreign_key: true
      t.timestamps
    end
  end
end
