class AddRpmToOptics < ActiveRecord::Migration[6.0]
  def change
    add_column :optics, :movement, :float, default: 0
    add_column :optics, :rpm, :integer, default: 0
    add_column :optics, :ads_time, :integer, default: 0
    add_column :optics, :sprint_to_fire, :integer, default: 0
    add_column :optics, :hip_fire_cone, :integer, default: 0
    add_column :optics, :bullet_velocity, :integer, default: 0
    add_column :optics, :bounce, :integer, default: 0
    add_column :optics, :vertical_recoil, :integer, default: 0
    add_column :optics, :horizontal_recoil, :integer, default: 0
    add_column :optics, :vertical_recoil_first_10, :integer, default: 0
    add_column :optics, :horizontal_recoil_first_10, :integer, default: 0
    add_column :optics, :vertical_recoil_first_20, :integer, default: 0
    add_column :optics, :horizontal_recoil_first_20, :integer, default: 0
    add_column :optics, :head_dmg_range_1, :integer, default: 0
    add_column :optics, :chest_dmg_range_1, :integer, default: 0
    add_column :optics, :stomach_dmg_range_1, :integer, default: 0
    add_column :optics, :limbs_dmg_range_1, :integer, default: 0
    add_column :optics, :head_dmg_range_2, :integer, default: 0
    add_column :optics, :chest_dmg_range_2, :integer, default: 0
    add_column :optics, :stomach_dmg_range_2, :integer, default: 0
    add_column :optics, :limbs_dmg_range_2, :integer, default: 0
    add_column :optics, :head_dmg_range_3, :integer, default: 0
    add_column :optics, :chest_dmg_range_3, :integer, default: 0
    add_column :optics, :stomach_dmg_range_3, :integer, default: 0
    add_column :optics, :limbs_dmg_range_3, :integer, default: 0
    add_column :optics, :head_dmg_range_4, :integer, default: 0
    add_column :optics, :chest_dmg_range_4, :integer, default: 0
    add_column :optics, :stomach_dmg_range_4, :integer, default: 0
    add_column :optics, :limbs_dmg_range_4, :integer, default: 0
    add_column :optics, :spread_image, :string, default: ''
    add_column :optics, :game, :string, default: ''
    add_column :optics, :note, :string, default: ''
    add_column :optics, :range_1, :float, default: 0
    add_column :optics, :range_2, :float, default: 0
    add_column :optics, :range_3, :float, default: 0
    add_column :optics, :range_3_used, :boolean, default: false
    add_column :optics, :range_4, :float, default: 0
    add_column :optics, :range_4_used, :boolean, default: false
    add_column :optics, :reload_time, :decimal
  end
end
