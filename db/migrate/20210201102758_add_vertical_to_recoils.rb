class AddVerticalToRecoils < ActiveRecord::Migration[6.0]
  def change

    add_column :recoils, :hip_fire_cone, :integer, default: 0
    add_column :recoils, :bounce, :integer, default: 0
    add_column :recoils, :vertical_recoil, :integer, default: 0
    add_column :recoils, :horizontal_recoil, :integer, default: 0
    add_column :recoils, :vertical_recoil_first_10, :integer, default: 0
    add_column :recoils, :horizontal_recoil_first_10, :integer, default: 0
    add_column :recoils, :vertical_recoil_first_20, :integer, default: 0
    add_column :recoils, :horizontal_recoil_first_20, :integer, default: 0
    add_column :recoils, :vertical_recoil_first_0, :integer, default: 0
    add_column :recoils, :horizontal_recoil_first_0, :integer, default: 0

  end
end
