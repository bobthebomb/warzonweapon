class CreateMuzzleRecoils < ActiveRecord::Migration[6.0]
  def change
    create_table :muzzle_recoils do |t|

      t.string :name

      t.references :muzzle, null: false, foreign_key: true
      t.timestamps
    end
  end
end
