class AddRpmToUnderbarrels < ActiveRecord::Migration[6.0]
  def change
    add_column :underbarrels, :movement, :float, default: 0
    add_column :underbarrels, :rpm, :integer, default: 0
    add_column :underbarrels, :ads_time, :integer, default: 0
    add_column :underbarrels, :sprint_to_fire, :integer, default: 0
    add_column :underbarrels, :hip_fire_cone, :integer, default: 0
    add_column :underbarrels, :bullet_velocity, :integer, default: 0
    add_column :underbarrels, :bounce, :integer, default: 0
    add_column :underbarrels, :vertical_recoil, :integer, default: 0
    add_column :underbarrels, :horizontal_recoil, :integer, default: 0
    add_column :underbarrels, :vertical_recoil_first_10, :integer, default: 0
    add_column :underbarrels, :horizontal_recoil_first_10, :integer, default: 0
    add_column :underbarrels, :vertical_recoil_first_20, :integer, default: 0
    add_column :underbarrels, :horizontal_recoil_first_20, :integer, default: 0
    add_column :underbarrels, :head_dmg_range_1, :integer, default: 0
    add_column :underbarrels, :chest_dmg_range_1, :integer, default: 0
    add_column :underbarrels, :stomach_dmg_range_1, :integer, default: 0
    add_column :underbarrels, :limbs_dmg_range_1, :integer, default: 0
    add_column :underbarrels, :head_dmg_range_2, :integer, default: 0
    add_column :underbarrels, :chest_dmg_range_2, :integer, default: 0
    add_column :underbarrels, :stomach_dmg_range_2, :integer, default: 0
    add_column :underbarrels, :limbs_dmg_range_2, :integer, default: 0
    add_column :underbarrels, :head_dmg_range_3, :integer, default: 0
    add_column :underbarrels, :chest_dmg_range_3, :integer, default: 0
    add_column :underbarrels, :stomach_dmg_range_3, :integer, default: 0
    add_column :underbarrels, :limbs_dmg_range_3, :integer, default: 0
    add_column :underbarrels, :head_dmg_range_4, :integer, default: 0
    add_column :underbarrels, :chest_dmg_range_4, :integer, default: 0
    add_column :underbarrels, :stomach_dmg_range_4, :integer, default: 0
    add_column :underbarrels, :limbs_dmg_range_4, :integer, default: 0
    add_column :underbarrels, :spread_image, :string, default: ''
    add_column :underbarrels, :game, :string, default: ''
    add_column :underbarrels, :note, :string, default: ''
    add_column :underbarrels, :range_1, :float, default: 0
    add_column :underbarrels, :range_2, :float, default: 0
    add_column :underbarrels, :range_3, :float, default: 0
    add_column :underbarrels, :range_3_used, :boolean, default: false
    add_column :underbarrels, :range_4, :float, default: 0
    add_column :underbarrels, :range_4_used, :boolean, default: false
    add_column :underbarrels, :reload_time, :decimal
  end
end
