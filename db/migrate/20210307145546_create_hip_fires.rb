class CreateHipFires < ActiveRecord::Migration[6.0]
  def change
    create_table :hip_fires do |t|
      t.float :yaxis
      t.float :xaxis
      t.references :hipfireable, polymorphic: true, null: false

      t.timestamps
    end
  end
end
