class AddRpmToWeapons < ActiveRecord::Migration[6.0]
  def change
    add_column :weapons, :rpm, :integer, default: 0
  end
end
