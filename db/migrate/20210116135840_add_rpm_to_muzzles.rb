class AddRpmToMuzzles < ActiveRecord::Migration[6.0]
  def change
    add_column :muzzles, :movement, :float, default: 0
    add_column :muzzles, :rpm, :integer, default: 0
    add_column :muzzles, :ads_time, :integer, default: 0
    add_column :muzzles, :sprint_to_fire, :integer, default: 0
    add_column :muzzles, :hip_fire_cone, :integer, default: 0
    add_column :muzzles, :bullet_velocity, :integer, default: 0
    add_column :muzzles, :bounce, :integer, default: 0
    add_column :muzzles, :vertical_recoil, :integer, default: 0
    add_column :muzzles, :horizontal_recoil, :integer, default: 0
    add_column :muzzles, :vertical_recoil_first_10, :integer, default: 0
    add_column :muzzles, :horizontal_recoil_first_10, :integer, default: 0
    add_column :muzzles, :vertical_recoil_first_20, :integer, default: 0
    add_column :muzzles, :horizontal_recoil_first_20, :integer, default: 0
    add_column :muzzles, :head_dmg_range_1, :integer, default: 0
    add_column :muzzles, :chest_dmg_range_1, :integer, default: 0
    add_column :muzzles, :stomach_dmg_range_1, :integer, default: 0
    add_column :muzzles, :limbs_dmg_range_1, :integer, default: 0
    add_column :muzzles, :head_dmg_range_2, :integer, default: 0
    add_column :muzzles, :chest_dmg_range_2, :integer, default: 0
    add_column :muzzles, :stomach_dmg_range_2, :integer, default: 0
    add_column :muzzles, :limbs_dmg_range_2, :integer, default: 0
    add_column :muzzles, :head_dmg_range_3, :integer, default: 0
    add_column :muzzles, :chest_dmg_range_3, :integer, default: 0
    add_column :muzzles, :stomach_dmg_range_3, :integer, default: 0
    add_column :muzzles, :limbs_dmg_range_3, :integer, default: 0
    add_column :muzzles, :head_dmg_range_4, :integer, default: 0
    add_column :muzzles, :chest_dmg_range_4, :integer, default: 0
    add_column :muzzles, :stomach_dmg_range_4, :integer, default: 0
    add_column :muzzles, :limbs_dmg_range_4, :integer, default: 0
    add_column :muzzles, :spread_image, :string, default: ''
    add_column :muzzles, :game, :string, default: ''
    add_column :muzzles, :note, :string, default: ''
    add_column :muzzles, :range_1, :float, default: 0
    add_column :muzzles, :range_2, :float, default: 0
    add_column :muzzles, :range_3, :float, default: 0
    add_column :muzzles, :range_3_used, :boolean, default: false
    add_column :muzzles, :range_4, :float, default: 0
    add_column :muzzles, :range_4_used, :boolean, default: false
    add_column :muzzles, :reload_time, :decimal
  end
end
