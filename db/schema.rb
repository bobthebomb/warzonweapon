# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_20_003809) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "blogs", force: :cascade do |t|
    t.string "title"
    t.text "text_area_1"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "featured", default: 0
    t.string "description", default: ""
    t.boolean "show_illustration", default: true
    t.string "slug"
    t.index ["slug"], name: "index_blogs_on_slug", unique: true
  end

  create_table "canon_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "canon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["canon_id"], name: "index_canon_recoils_on_canon_id"
  end

  create_table "canons", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_canons_on_weapon_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "grip_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "grip_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["grip_id"], name: "index_grip_recoils_on_grip_id"
  end

  create_table "grips", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_grips_on_weapon_id"
  end

  create_table "hip_fires", force: :cascade do |t|
    t.float "yaxis"
    t.float "xaxis"
    t.string "hipfireable_type", null: false
    t.bigint "hipfireable_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["hipfireable_type", "hipfireable_id"], name: "index_hip_fires_on_hipfireable_type_and_hipfireable_id"
  end

  create_table "laser_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "laser_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["laser_id"], name: "index_laser_recoils_on_laser_id"
  end

  create_table "lasers", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_lasers_on_weapon_id"
  end

  create_table "munition_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "munition_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["munition_id"], name: "index_munition_recoils_on_munition_id"
  end

  create_table "munitions", force: :cascade do |t|
    t.string "name"
    t.integer "magazine"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_munitions_on_weapon_id"
  end

  create_table "muzzle_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "muzzle_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["muzzle_id"], name: "index_muzzle_recoils_on_muzzle_id"
  end

  create_table "muzzles", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_muzzles_on_weapon_id"
  end

  create_table "optic_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "optic_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["optic_id"], name: "index_optic_recoils_on_optic_id"
  end

  create_table "optics", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_optics_on_weapon_id"
  end

  create_table "perks", force: :cascade do |t|
    t.string "name"
    t.text "effect"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "reload_time", default: 0.0
    t.index ["weapon_id"], name: "index_perks_on_weapon_id"
  end

  create_table "recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "hip_fire_cone", default: 0
    t.integer "bounce", default: 0
    t.integer "vertical_recoil", default: 0
    t.integer "horizontal_recoil", default: 0
    t.integer "vertical_recoil_first_10", default: 0
    t.integer "horizontal_recoil_first_10", default: 0
    t.integer "vertical_recoil_first_20", default: 0
    t.integer "horizontal_recoil_first_20", default: 0
    t.integer "vertical_recoil_first_0", default: 0
    t.integer "horizontal_recoil_first_0", default: 0
    t.index ["weapon_id"], name: "index_recoils_on_weapon_id"
  end

  create_table "setups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.float "firing_movement", default: 0.0
    t.integer "magazine", default: 0
    t.string "meta_id", default: ""
    t.boolean "active", default: false
    t.string "attachement_names", array: true
    t.float "burst", default: 0.0
  end

  create_table "stock_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "stock_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["stock_id"], name: "index_stock_recoils_on_stock_id"
  end

  create_table "stocks", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_stocks_on_weapon_id"
  end

  create_table "types", force: :cascade do |t|
    t.string "name"
    t.string "game"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "underbarrel_recoils", force: :cascade do |t|
    t.string "name"
    t.bigint "underbarrel_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "hip_fire_cone", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.float "vertical_recoil_first_0", default: 0.0
    t.float "horizontal_recoil_first_0", default: 0.0
    t.index ["underbarrel_id"], name: "index_underbarrel_recoils_on_underbarrel_id"
  end

  create_table "underbarrels", force: :cascade do |t|
    t.string "name"
    t.bigint "weapon_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "movement", default: 0.0
    t.float "rpm", default: 0.0
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "hip_fire_cone", default: 0.0
    t.float "bullet_velocity", default: 0.0
    t.float "bounce", default: 0.0
    t.float "vertical_recoil", default: 0.0
    t.float "horizontal_recoil", default: 0.0
    t.float "vertical_recoil_first_10", default: 0.0
    t.float "horizontal_recoil_first_10", default: 0.0
    t.float "vertical_recoil_first_20", default: 0.0
    t.float "horizontal_recoil_first_20", default: 0.0
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.float "reload_time", default: 0.0
    t.float "ads_movement", default: 0.0
    t.index ["weapon_id"], name: "index_underbarrels_on_weapon_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "admin", default: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "weapons", force: :cascade do |t|
    t.string "name"
    t.bigint "type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "rpm", default: 0.0
    t.integer "magazine", default: 0
    t.decimal "reload_time", default: "0.0"
    t.float "ads_time", default: 0.0
    t.float "sprint_to_fire", default: 0.0
    t.float "movement", default: 0.0
    t.float "ads_movement", default: 0.0
    t.decimal "hip_fire_cone", default: "0.0"
    t.decimal "bullet_velocity", default: "0.0"
    t.decimal "bounce", default: "0.0"
    t.decimal "vertical_recoil", default: "0.0"
    t.decimal "horizontal_recoil", default: "0.0"
    t.decimal "vertical_recoil_first_10", default: "0.0"
    t.decimal "horizontal_recoil_first_10", default: "0.0"
    t.decimal "vertical_recoil_first_20", default: "0.0"
    t.decimal "horizontal_recoil_first_20", default: "0.0"
    t.integer "head_dmg_range_1", default: 0
    t.integer "chest_dmg_range_1", default: 0
    t.integer "stomach_dmg_range_1", default: 0
    t.integer "limbs_dmg_range_1", default: 0
    t.integer "head_dmg_range_2", default: 0
    t.integer "chest_dmg_range_2", default: 0
    t.integer "stomach_dmg_range_2", default: 0
    t.integer "limbs_dmg_range_2", default: 0
    t.integer "head_dmg_range_3", default: 0
    t.integer "chest_dmg_range_3", default: 0
    t.integer "stomach_dmg_range_3", default: 0
    t.integer "limbs_dmg_range_3", default: 0
    t.integer "head_dmg_range_4", default: 0
    t.integer "chest_dmg_range_4", default: 0
    t.integer "stomach_dmg_range_4", default: 0
    t.integer "limbs_dmg_range_4", default: 0
    t.string "spread_image", default: ""
    t.string "game", default: ""
    t.string "note", default: ""
    t.float "range_1", default: 0.0
    t.float "range_2", default: 0.0
    t.float "range_3", default: 0.0
    t.boolean "range_3_used", default: false
    t.float "range_4", default: 0.0
    t.boolean "range_4_used", default: false
    t.text "muzzles_notes", default: ""
    t.text "canons_notes", default: ""
    t.text "lasers_notes", default: ""
    t.text "optics_notes", default: ""
    t.text "stocks_notes", default: ""
    t.text "underbarrels_notes", default: ""
    t.text "munitions_notes", default: ""
    t.text "grips_notes", default: ""
    t.text "perks_notes", default: ""
    t.float "burst", default: 0.0
    t.index ["type_id"], name: "index_weapons_on_type_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "canon_recoils", "canons"
  add_foreign_key "canons", "weapons"
  add_foreign_key "grip_recoils", "grips"
  add_foreign_key "grips", "weapons"
  add_foreign_key "laser_recoils", "lasers"
  add_foreign_key "lasers", "weapons"
  add_foreign_key "munition_recoils", "munitions"
  add_foreign_key "munitions", "weapons"
  add_foreign_key "muzzle_recoils", "muzzles"
  add_foreign_key "muzzles", "weapons"
  add_foreign_key "optic_recoils", "optics"
  add_foreign_key "optics", "weapons"
  add_foreign_key "perks", "weapons"
  add_foreign_key "recoils", "weapons"
  add_foreign_key "stock_recoils", "stocks"
  add_foreign_key "stocks", "weapons"
  add_foreign_key "underbarrel_recoils", "underbarrels"
  add_foreign_key "underbarrels", "weapons"
  add_foreign_key "weapons", "types"
end
